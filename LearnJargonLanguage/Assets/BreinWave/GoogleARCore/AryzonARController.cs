﻿//-----------------------------------------------------------------------
// <copyright file="HelloARController.cs" company="Google">
//
// Copyright 2017 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.Examples.HelloAR
{
    using System.Collections.Generic;
    using BreinWave.Singleton;
    using GoogleARCore;
    using GoogleARCore.Examples.AugmentedImage;
    //using GoogleARCore.Examples.Common;
    using UnityEngine;
    

#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    // using Input = InstantPreviewInput;
#endif

    /// <summary>
    /// Controls the HelloAR example.
    /// </summary>
    public class AryzonARController : Singleton<AryzonARController>
    {

        #region Plane Tracking Related
        /// <summary>
        /// A model to place when a raycast from a user touch hits a plane.
        /// </summary>
        public GameObject centerObject = null;


        /// <summary>
        /// The rotation in degrees need to apply to model when the Andy model is placed.
        /// </summary>
        private const float k_ModelRotation = 180.0f;

        /// <summary>
        /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
        private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();
        #endregion

        #region Marker Related

        public GameObject markerObject;
        public WorldCenter_Visualizer WorldCenter_VisualizerPrefab;

        /// <summary>
        /// The overlay containing the fit to scan user guide.
        /// </summary>
        private Dictionary<int, WorldCenter_Visualizer> m_Visualizers
            = new Dictionary<int, WorldCenter_Visualizer>();

        private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();
        #endregion

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
        /// </summary>
        private bool m_IsQuitting = false;


        private bool placedOnce = false;


        public void Start()
        {
            var temp = Resources.Load("WorldCenter_Visualizer") as GameObject;
            WorldCenter_VisualizerPrefab = temp.GetComponent<WorldCenter_Visualizer>();
        }

        /// <summary>
        /// The Unity Update() method.
        /// </summary>
        public void Update()
        {
            //PlaneTracking();
            _UpdateApplicationLifecycle();
            MarkerTracking();
                                           
        }
        #region Plane Tracking and Object Placement
        private void PlaneTracking()
        {
            _UpdateApplicationLifecycle();

            if (centerObject == null)
                return;

            // Hide snackbar when currently tracking at least one plane.
            Session.GetTrackables<DetectedPlane>(m_AllPlanes);
            for (int i = 0; i < m_AllPlanes.Count; i++)
            {
                if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
                {
                    break;
                }
            }
        }

        public void AnchorMarkerObject()
        {
            // If the CenterObject is placed, we are done with this update
            if (placedOnce)
            {
                return;
            }

            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            if (Frame.Raycast(markerObject.transform.position, -markerObject.transform.up, out hit, 3, raycastFilter))
            {
                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((hit.Trackable is DetectedPlane) &&
                    Vector3.Dot(markerObject.transform.position - hit.Pose.position,
                        hit.Pose.rotation * Vector3.up) < 0)
                {
                    Debug.Log("Hit at back of the current DetectedPlane");
                }
                else
                {
                    // Choose the Andy model for the Trackable that got hit.
                    GameObject prefab;
                    if (hit.Trackable is FeaturePoint)
                    {
                        prefab = centerObject;
                    }
                    else
                    {
                        prefab = centerObject;
                    }

                    // Instantiate Andy model at the hit pose.
                    var t_centerObject = Instantiate(prefab, hit.Pose.position, hit.Pose.rotation);

                    // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                    //andyObject.transform.Rotate(0, k_ModelRotation, 0, Space.Self);

                    // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
                    // world evolves.
                    var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                    // Make Andy model a child of the anchor.
                    t_centerObject.transform.parent = anchor.transform;

                    placedOnce = true;
                }
            }
        }


        /// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
            }
            else
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        #endregion


        #region Marker Tracking
        public void MarkerTracking()
        {

            // Check that motion tracking is tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                return;
            }

            // Get updated augmented images for this frame.
            Session.GetTrackables<AugmentedImage>(m_TempAugmentedImages, TrackableQueryFilter.Updated);

            // Create visualizers and anchors for updated augmented images that are tracking and do not previously
            // have a visualizer. Remove visualizers for stopped images.
            foreach (var image in m_TempAugmentedImages)
            {
                WorldCenter_Visualizer worldCenterVisualizer = null;
                m_Visualizers.TryGetValue(image.DatabaseIndex, out worldCenterVisualizer);
                if (image.TrackingState == TrackingState.Tracking && worldCenterVisualizer == null)
                {
                    // Create an anchor to ensure that ARCore keeps tracking this augmented image.
                    Anchor anchor = image.CreateAnchor(image.CenterPose);
                    worldCenterVisualizer = (WorldCenter_Visualizer)Instantiate(WorldCenter_VisualizerPrefab, anchor.transform.position, anchor.transform.rotation,anchor.transform);
#if UNITY_ANDROID && BreinWave_GoogleARCore
                    worldCenterVisualizer.thisAnchor = anchor;
#endif
                    worldCenterVisualizer.transform.localPosition = Vector3.zero;
                    worldCenterVisualizer.transform.localRotation = Quaternion.identity;
                    m_Visualizers.Add(image.DatabaseIndex, worldCenterVisualizer);

                    worldCenterVisualizer.SetObjectHolderLocation();
                }
                else if (image.TrackingState == TrackingState.Stopped && worldCenterVisualizer != null)
                {
                    m_Visualizers.Remove(image.DatabaseIndex);
                    GameObject.Destroy(worldCenterVisualizer.gameObject);
                }
            }
        }
    

#endregion
    /// <summary>
    /// Actually quit the application.
    /// </summary>
    private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }
    }
}
