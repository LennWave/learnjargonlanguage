﻿using BreinWave.Devices;
using BreinWave.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarManager : MonoBehaviour {

    public Transform marker;

    void Start()
    {
#if UNITY_WSA
        if (PhotonNetwork.connectedAndReady)
        {
            StartCoroutine(UpdatePosition());
        }
#endif
    }

    IEnumerator UpdatePosition()
    {
        while (true)
        {            
            this.transform.position = marker.position;
            this.transform.rotation = marker.rotation;
            yield return new WaitForSeconds(3);
        }
    }
}
