﻿
#if BreinWave_Photon
using BreinWave.Game;

using UnityEngine;
using UnityEngine.SceneManagement;


public class NetworkSpawn : Photon.MonoBehaviour
{
    //public Transform playerPrefab;
    //public Transform bombPrefab;
    //public string deviceString;
    private MultiplayerLobbyController mplController;


    public void Awake()
    {
        Debug.Log("Starting NetworkSpawn");
        // in case we started this demo with the wrong scene being active, simply load the menu scene
        mplController = MultiplayerLobbyController.Instance;
        if (!PhotonNetwork.connected)
        {
            Debug.Log("Menu scene will be loaded");
            SceneManager.LoadScene(mplController.LobbySceneName);
            return;
        }

        //Camera.main.clearFlags = CameraClearFlags.Skybox;

    }

    private void Start()
    {

        foreach (Camera cam in Camera.allCameras)
        {
#if !VUFORIA
            cam.clearFlags = CameraClearFlags.Skybox;
#endif
            Debug.Log(cam);
        }

    }

    public void OnMasterClientSwitched(PhotonPlayer player)
    {
        Debug.Log("OnMasterClientSwitched: " + player);

        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(mplController.LobbySceneName);
        //string message;
        //InRoomChat chatComponent = GetComponent<InRoomChat>();  // if we find a InRoomChat component, we print out a short message

        //if (chatComponent != null)
        //{
        //    // to check if this client is the new master...
        //    if (player.IsLocal)
        //    {
        //        message = "You are Master Client now.";
        //    }
        //    else
        //    {
        //        message = player.NickName + " is Master Client now.";
        //    }


        //    chatComponent.AddLine(message); // the Chat method is a RPC. as we don't want to send an RPC and neither create a PhotonMessageInfo, lets call AddLine()
        //}
    }

    public void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom (local)");

        // back to main menu
        SceneManager.LoadScene(mplController.LobbySceneName);
    }


    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDisconnectedFromPhoton");

        // back to main menu
        SceneManager.LoadScene(mplController.LobbySceneName);
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("OnPhotonInstantiate " + info.sender);    // you could use this info to store this or react
    }

    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerConnected: " + player);

    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Debug.Log("OnPlayerDisconneced: " + player);
    }

    public void OnFailedToConnectToPhoton()
    {
        Debug.Log("OnFailedToConnectToPhoton");


        // back to main menu
        SceneManager.LoadScene(mplController.LobbySceneName);
    }
}
#endif
