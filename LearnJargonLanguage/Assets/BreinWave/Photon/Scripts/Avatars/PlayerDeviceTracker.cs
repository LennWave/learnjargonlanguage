﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreinWave.Singleton;

namespace BreinWave.Unity.Devices
{
    public enum DeviceNames
    {
        None,
        Aryzon,
        Desktop,
        Hololens,
        Host,
        VR
    };

    public class PlayerDeviceTracker : Singleton<PlayerDeviceTracker>
    {
        public DeviceNames playerDevice = DeviceNames.None;

        public override void Awake()
        {
            Debug.Log("Loaded Player device tracker : " +  playerDevice);

            base.Awake();

            DontDestroyOnLoad(this.gameObject);
            // y do this?
            this.tag = typeof(PlayerDeviceTracker).Name;
        }
    }
}