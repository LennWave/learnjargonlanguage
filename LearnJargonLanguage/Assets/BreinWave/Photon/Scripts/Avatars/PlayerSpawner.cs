﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    public Transform playerPrefab;

	// Use this for initialization
	void Start ()
    {
        if (PhotonNetwork.connectedAndReady)
        {
            PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity, 0);
        }
	}
}
