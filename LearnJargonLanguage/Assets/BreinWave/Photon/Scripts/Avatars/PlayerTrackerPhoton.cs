﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrackerPhoton : Photon.MonoBehaviour {

    public PhotonView photonView;
    public TextMesh visorName;
    public Transform cameraTransform;
    public Vector3 localPosCorrect;
    public Quaternion localRotCorrect;
    Vector3 moveSpeed = Vector3.zero;
    bool firstSet = true;


	// Use this for initialization
	void Start ()
    {
        photonView = GetComponent<PhotonView>();
        gameObject.name = visorName.text = photonView.owner.NickName;
        transform.parent = GameObject.FindGameObjectWithTag("PlayerAvatarManager").transform;

        if (photonView.isMine)
        {
            if (Camera.main == null)
            {
                cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
            }
            else
            {
                cameraTransform = Camera.main.transform;
            }
            transform.position = cameraTransform.position;
            transform.rotation = cameraTransform.rotation;
//#if UNITY_WSA
//            VuforiaBehaviour.Instance.enabled = true;
//#endif
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(photonView.isMine)
        {
            transform.position = cameraTransform.position;
            transform.rotation = cameraTransform.rotation;
        }
        else
        {
            if(firstSet)
            {
                firstSet = false;
                transform.localPosition = localPosCorrect;
                transform.localRotation = localRotCorrect;
            }
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, localPosCorrect, ref moveSpeed, 1);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, localRotCorrect, 360f * Time.deltaTime);
        }
	}

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if( stream.isWriting)
        {
            stream.SendNext(transform.localPosition);
            stream.SendNext(transform.localRotation);

        }
        else if(stream.isReading)
        {
            localPosCorrect = (Vector3)stream.ReceiveNext();
            localRotCorrect = (Quaternion)stream.ReceiveNext();
        }
    }
}
