﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BreinWave.Menu
{
    public class CreateGameScreen : MenuScreen
    {

        public override string Title { get { return "Create game"; } }

        public InputField RoomnameInput;

        public InputField PlayernameInput;


        private string RoomName
        {
            get
            {
                return RoomnameInput.text;
            }
            set
            {
                RoomnameInput.text = value;
            }
        }

        private string PlayerName
        {
            get
            {
                return PlayernameInput.text;
            }
            set
            {
                PlayernameInput.text = value;
            }
        }

#if !BreinWave_Photon
        public override MenuScreen GoBack()
        {
            // Does nothing. Is only here because else an error will complain that this function is not implemented.
            return null;
        }
#endif

#if BreinWave_Photon
        public override MenuScreen GoBack()
        {
            return MultiplayerLobbyController.Instance.LobbyCanvas.GetScreen<MainMenuScreen>() as MenuScreen;
        }

        public override void Open()
        {
            PlayerName = PhotonNetwork.playerName;
            base.Open();
        }

        public override void Close()
        {
            if (!string.IsNullOrEmpty(PlayerName))
                PhotonNetwork.playerName = PlayerName;

            base.Close();
        }

        public void CreateRoom()
        {
            if (string.IsNullOrEmpty(RoomName))
                MultiplayerLobbyController.Instance.LobbyCanvas.PopUpDialog.Show("De room name field can not be empty");


            if (string.IsNullOrEmpty(PlayerName))
                MultiplayerLobbyController.Instance.LobbyCanvas.PopUpDialog.Show("De player name field can not be empty");

            PhotonNetwork.playerName = PlayerName;
            MultiplayerLobbyController.Instance.roomName = RoomName;

            PhotonNetwork.CreateRoom(this.RoomName, new RoomOptions() { MaxPlayers = 10 }, null);

        }
#endif
    }
}
