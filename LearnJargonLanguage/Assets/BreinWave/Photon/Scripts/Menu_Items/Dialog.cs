﻿using BreinWave.Singleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace BreinWave.Menu
{
    public delegate void DialogButtonClicked();

    public class Dialog : MonoBehaviour
    {

        public Text TitleText;

        public Text ContentText;

        public Button OkButton;

        public Button CancelButton;

        private DialogButtonClicked ok;

        private DialogButtonClicked cancel;

        public GameObject Frame;

        //public override void Awake()
        //{
        //    base.Awake();
        //    // TODO - make singelton work.
        //    //Frame = Instantiate(Resources.Load("Prefabs/MultiplayerCanvas", typeof(GameObject))) as GameObject;
        //}

        public void OkButtonExecute()
        {
            if (ok != null)
                ok.Invoke();


            Frame.SetActive(false);
            StopCoroutine("Disappear");
        }
        public void CancelButtonExecute()
        {
            if (cancel != null)
                cancel.Invoke();


            Frame.SetActive(false);
            StopCoroutine("Disappear");
        }

        public void Show(string txt, string title = "Let op!", double disappearTime = 0, DialogButtonClicked OKButtonClicked = null, DialogButtonClicked CancelButtonClicked = null)
        {
            ContentText.text = txt;
            TitleText.text = title;

            if (disappearTime > 0)
            {
                this.StartCoroutine(Disappear(disappearTime));
            }

            ok = OKButtonClicked;
            cancel = CancelButtonClicked;

            //OkButton.gameObject.SetActive(ok != null);
            CancelButton.gameObject.SetActive(cancel != null);

            Frame.SetActive(true);
        }

        IEnumerator Disappear(double time)
        {

            yield return new WaitForSeconds((float)time);

            Frame.SetActive(false);
        }

    }
}