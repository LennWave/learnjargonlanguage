﻿
using BreinWave.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BreinWave.Menu
{

    public class JoinGameScreen : MenuScreen
    {
        public override string Title { get { return "Join game"; } }

        public InputField PlayernameInput;

        public RectTransform Content;

        private string PlayerName
        {
            get
            {
                return PlayernameInput.text;
            }
            set
            {
                PlayernameInput.text = value;
            }
        }
        
        public RoomListItem UIPrefab;

#if !BreinWave_Photon
        public override MenuScreen GoBack()
        {
            // Does nothing. Is only here because else an error will complain that this function is not implemented.
            return null;
        }
#endif

#if BreinWave_Photon
        public override MenuScreen GoBack()
        {
            return MultiplayerLobbyController.Instance.LobbyCanvas.GetScreen<MainMenuScreen>() as MenuScreen;
        }

        public override void Open()
        {
            PlayerName = PhotonNetwork.playerName;

            CleanContent();
            foreach (RoomInfo room in PhotonNetwork.GetRoomList())
            {
                RoomListItem card = GameObject.Instantiate<RoomListItem>(UIPrefab, Content);
                card.SetValues(room);
            } 

            base.Open();
        }

        private void CleanContent()
        {
            while(Content.childCount > 0)
            {
                Destroy(Content.GetChild(0).gameObject);
            }

        }

#endif
    }
}

