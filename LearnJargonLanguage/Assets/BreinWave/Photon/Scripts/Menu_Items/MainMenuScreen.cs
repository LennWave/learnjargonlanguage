﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreinWave.Menu
{
    public class MainMenuScreen : MenuScreen
    {
        public override string Title { get { return "Main menu"; } }

        public override MenuScreen GoBack()
        {
            return null;
        }
    }
}