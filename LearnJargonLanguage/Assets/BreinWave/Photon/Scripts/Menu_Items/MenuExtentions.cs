﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreinWave.Menu
{
    public static class MenuExtentions
    {

        public static T OpenScreen<T>(this MenuScreen screen, List<MenuScreen> screens) where T : MenuScreen
        {
            T val = null;

            foreach (var s in screens)
            {
                if (s.GetType() == typeof(T))
                {
                    val = s as T;
                    s.Open();
                    //ToolbarMenu.SetValues(s);
                }
                else if (s.IsOpen)
                {
                    s.Close();
                }
            }

            return val as T;
        }

        public static T GetScreen<T>(this MenuScreen screen, List<MenuScreen> screens) where T : MenuScreen
        {
            foreach (var s in screens)
            {
                if (s.GetType() == typeof(T))
                {
                    return s as T;
                }
            }
            return null;
        }

    }
}