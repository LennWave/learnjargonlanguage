﻿#if BreinWave_Photon
#endif
using BreinWave.Game;
using System.Collections.Generic;
using UnityEngine;


namespace BreinWave.Menu
{
    public class MultiplayerLobbyCanvas : MonoBehaviour
    {


#pragma warning disable IDE0044 // Add readonly modifier
        private string deviceString;
#pragma warning restore IDE0044 // Add readonly modifier

        public List<MenuScreen> AllScreens;
        public Canvas thisCanvas;
        public Toolbar ToolbarMenu;
        public Dialog PopUpDialog;

        public T GetScreen<T>() where T : MenuScreen
        {
            foreach (var s in AllScreens)
            {
                if (s.GetType() == typeof(T))
                {
                    return s as T;
                }
            }
            return null;
        }
#if BreinWave_Photon
        public void OpenScreen(MenuScreen screen)
        {
            foreach (var s in AllScreens)
            {
                if (s == screen)
                {
                    s.Open();
                    ToolbarMenu.SetValues(s);
                }
                else if (s.IsOpen)
                {
                    s.Close();
                }
            }
        }

        public void EnsureExists()
        {
            Debug.Log("Ensured");
        }


        public void Start()
        {


#if UNITY_ANDROID
            thisCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
#endif

#if BreinWave_HoloToolkit

            var container = new GameObject("Container");
            this.transform.SetParent(container.transform);
            this.transform.localPosition = new Vector3(0, 0, 5);
            this.transform.localEulerAngles = Vector3.zero;

            var follow = container.AddComponent<UIFollow>();
            follow.DoFollow = true;
            follow.RotationalOffset = new Vector2(.15f, .15f);
            follow.Target = GameManager.Instance.CurrentDevice.GetVirtualDevice().transform;
#endif


            CreateGameScreen create = GetScreen<CreateGameScreen>();
            create.PlayernameInput.text = PhotonNetwork.playerName;
            create.RoomnameInput.text = MultiplayerLobbyController.Instance.roomName;

            JoinGameScreen join = GetScreen<JoinGameScreen>();
            join.PlayernameInput.text = PhotonNetwork.playerName;
            //Debug.Log("Menu found :  " + GetScreen<MainMenuScreen>().GetType().Name);
            //Debug.Log("Menu found :  " + GetScreen<JoinGameScreen>().GetType().Name);
            //Debug.Log("Menu found :  " + GetScreen<CreateGameScreen>().GetType().Name);
        }
#endif
    }

}
