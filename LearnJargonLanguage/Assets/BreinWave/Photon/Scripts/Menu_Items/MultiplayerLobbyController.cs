﻿#if BreinWave_Photon
using BreinWave.Singleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreinWave.Menu;
using BreinWave.Game;
using System;
using UnityEditor;

public class MultiplayerLobbyController : Singleton<MultiplayerLobbyController>
{

    private MultiplayerLobbyCanvas _lobbyCanvas;
    Coroutine routine = null;
    public MultiplayerLobbyCanvas LobbyCanvas
    {
        get
        {
            if (_lobbyCanvas == null)
                _lobbyCanvas = GetCanvas();
            return _lobbyCanvas;
        }
    }

    private MultiplayerLobbyCanvas GetCanvas()
    {
        Debug.Log("Instantiated Multiplayercanvas");
        var test = Resources.Load("MultiplayerCanvas", typeof(MultiplayerLobbyCanvas));
        return Instantiate(test) as MultiplayerLobbyCanvas;
    }


    public bool AutomaticlyJoinGame;
    public bool HostAutomaticallyCreateRoom;

    public string deviceString;
    public string roomName = "InnovationRoom";

    private bool connectFailed = false;

    public string LobbySceneName = "ConfigurationMenu";

    public string GameSceneName = "SampleScene";


    //private string errorDialog;
    //private double timeToClearDialog;

    public string ErrorDialog
    {
        set
        {
            LobbyCanvas.PopUpDialog.Show(value, "Error", 4);
        }
    }

    public override void Awake()
    {
        //enabled = false;
        base.Awake();

        //Debug.LogError("This script does not belong in this project.");
    }

    public void Start()
    {
        //LobbyCanvas.EnsureExists();
        //MainMenuScreen screen = LobbyCanvas.OpenScreen<MainMenuScreen>();

        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;
        //PhotonNetwork.autoJoinLobby = false;

        // the following line checks if this client was just created (and not yet online). if so, we connect
        if (PhotonNetwork.connectionStateDetailed == ClientState.PeerCreated)
        {
            // Connect to the photon master-server. We/ use the settings saved in PhotonServerSettings (a .asset file in this project)
            PhotonNetwork.ConnectUsingSettings("0.9");
        }
        var test = GameManager.Instance.CurrentProfile;
        deviceString = GameManager.Instance.CurrentProfile.PlayerName;

        // generate a name for this player, if none is assigned yet
        if (string.IsNullOrEmpty(PhotonNetwork.playerName))
        {
            PhotonNetwork.playerName = deviceString + "#" + UnityEngine.Random.Range(1, 10000).ToString();
        }


    }

    //public void Update()
    //{
    //    //try
    //    //{
    //    //        PhotonNetwork.JoinOrCreateRoom(roomName, new RoomOptions(), new TypedLobby());
    //    //    enabled = false;
    //    //    return;

    //    //    var roomList = PhotonNetwork.GetRoomList();
    //    //    if (roomList[0].Name == roomName)
    //    //    {

    //    //    }

    //    //}
    //    //catch(Exception e)
    //    //{
    //    //}
    //}

    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
    }

    public void OnPhotonCreateRoomFailed()
    {
        ErrorDialog = "Error: Can't create room (room name maybe already used).";
        Debug.Log("OnPhotonCreateRoomFailed got called. This can happen if the room exists (even if not visible). Try another room name.");
    }

    public void OnPhotonJoinRoomFailed(object[] cause)
    {
        ErrorDialog = "Error: Can't join room (full or unknown room name). " + cause[1];
        Debug.Log("OnPhotonJoinRoomFailed got called. This can happen if the room is not existing or full or closed.");
    }

    public void OnPhotonRandomJoinFailed()
    {
        ErrorDialog = "Error: Can't join random room (none found).";
        Debug.Log("OnPhotonRandomJoinFailed got called. Happens if no room is available (or all full or invisible or closed). JoinrRandom filter-options can limit available rooms.");
    }

    public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
        PhotonNetwork.LoadLevel(GameSceneName);
    }


    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from Photon.");
    }

    public void OnFailedToConnectToPhoton(object parameters)
    {
        this.connectFailed = true;
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.ServerAddress);
    }

    public void OnConnectedToMaster()
    {
        Debug.Log("As OnConnectedToMaster() got called, the PhotonServerSetting.AutoJoinLobby must be off. Joining lobby by calling PhotonNetwork.JoinLobby().");
        PhotonNetwork.JoinLobby();
    }

    public void OnJoinedLobby()
    {
        LobbyCanvas.EnsureExists();
    }

    IEnumerator WaitForRoom()
    {
        bool roomFound = false;
        int n = 0;
        while (!roomFound || n > 10)
        {

            var roomList = PhotonNetwork.GetRoomList();
            for (int i = 0; i < roomList.Length; i++)
            {
                if (roomList[i].Name == roomName)
                {
                    roomFound = true;
                    PhotonNetwork.JoinRoom(roomName);
                    break;
                }
            }
            n++;
            yield return new WaitForSeconds(5.0f);
        }
        routine = null;
    }

}
#endif