﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListItem : MonoBehaviour
{
    public Text TitleText;
    public Text InfoText;

    private RoomInfo data;
#if BreinWave_Photon
    public void Join()
    {
        PhotonNetwork.JoinRoom(data.Name);
    }

    public void SetValues(RoomInfo info)
    {
        data = info;

        TitleText.text = info.Name;
        InfoText.text = string.Format("Players {0}/{1}", info.PlayerCount, info.MaxPlayers);


    }
#endif

}

