﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BreinWave.Menu
{
    public class Toolbar : MonoBehaviour
    {
        public Text titleText;

        public Button BackButton;

        private MenuScreen _backScreen;
        public MenuScreen BackScreen
        {
            set
            {
                _backScreen = value;

                if(value != null)
                    BackButton.gameObject.SetActive(true);
                else
                    BackButton.gameObject.SetActive(false);
            }
        }
#if BreinWave_Photon
        public void SetValues(MenuScreen screen)
        {
            BackScreen = screen.GoBack();

            titleText.text = screen.Title;

        }

        public void Back()
        {
            MultiplayerLobbyController.Instance.LobbyCanvas.OpenScreen(_backScreen);
        }
#endif


    }
}

