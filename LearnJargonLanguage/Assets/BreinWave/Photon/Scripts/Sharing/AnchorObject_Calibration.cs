﻿
using BreinWave.Game;
#if BreinWave_HoloToolkit
using HoloToolkit.Unity.InputModule;
#endif
using UnityEngine;

public class AnchorObject_Calibration : MonoBehaviour
{

    public Transform cockpitTransform, avatarManagerTransform;
    public GameObject calibrateCanvas;
    bool isVisible, headInPosition;
    public Vector3 offset, coPilotOffset;
    Vector3 cockPitPosition;
    public float distanceInCM, height;
    float distanceInMeters;
#if  BreinWave_HoloToolkit
    public HandDraggable handDraggable;
#endif
    public Collider collider;
    private Transform cameraTransform;

    // Use this for initialization
    void Start()
    {
        //if !firsttime
        //this.transform.position = SavePlayerPrefs.Instance.SavedCalibration;
        cockpitTransform.gameObject.SetActive(false);
        distanceInMeters = distanceInCM / 100;
        calibrateCanvas.SetActive(false);
        if(Camera.main == null)
        {
            cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
        }
        else
        {
            cameraTransform = Camera.main.transform;
        }
    }

    private void Update()
    {
        headInPosition = DetermineHeadPosition();
#if BreinWave_HoloToolkit
        handDraggable.IsDraggingEnabled = collider.enabled = !headInPosition;
#endif

        if(headInPosition && !isVisible && !calibrateCanvas.activeSelf) // If the user's head is in position then set the canvas active if it is not already active
        {
            isVisible = true;
            calibrateCanvas.SetActive(true);
            return;
        }
        else if (!headInPosition && isVisible && calibrateCanvas.activeSelf) // If the user's head is in position then set the canvas inactive if it is not already active
        {
            isVisible = false;
            calibrateCanvas.SetActive(false);
            return;
        }
    }


    public bool DetermineHeadPosition()
    {
        Vector3 headPosXZ = cameraTransform.position;
        headPosXZ.y = 0;
        Vector3 seatPosXZ = transform.position;
        seatPosXZ.y = 0;

        if(Vector3.Distance(headPosXZ, seatPosXZ) < distanceInMeters)
        {
            return true;
        }
        return false;
    }


    public void StartCalibration()
    {
#if UNITY_WSA
        cockpitTransform.rotation = transform.rotation;
        //cockpitTransform.eulerAngles = new Vector3(cockpitTransform.eulerAngles.x, cockpitTransform.eulerAngles.y + 180f, cockpitTransform.eulerAngles.z);
        cockPitPosition = transform.TransformPoint(offset);
        cockpitTransform.position = cockPitPosition;
        transform.localScale = Vector3.one * 1.2f;
#endif

#if UNITY_ANDROID
        //cockpitTransform.position = cameraTransform.position;
        //Vector3 camForward = Vector3.ProjectOnPlane(cameraTransform.forward, Vector3.up);
        //cockpitTransform.forward = camForward;
        //cockpitTransform.up = Vector3.up;
        //coPilotOffset.y -= height;
        //var temp = cockpitTransform.TransformPoint(coPilotOffset);
        //cockpitTransform.position = temp;
#endif

        cockpitTransform.gameObject.SetActive(true);
        avatarManagerTransform.position = cockpitTransform.position;
        avatarManagerTransform.rotation = cockpitTransform.rotation;
        this.gameObject.SetActive(false);

        GameManager.Instance.CurrentDevice.GetVirtualDevice();

    }

    public void Recalibrate()
    {
        cockpitTransform.gameObject.SetActive(false);
    }
}
