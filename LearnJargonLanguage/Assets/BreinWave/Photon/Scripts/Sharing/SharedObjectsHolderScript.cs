﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_WSA
using HoloToolkit.Unity.InputModule.Utilities.Interactions;
#endif

public class SharedObjectsHolderScript : MonoBehaviour {

    [Tooltip("This PhotonView will be taken over by the MasterClient.")]
    public PhotonView photonView;

    [Tooltip("Array of GameObjects that share a space on the MasterClient and the Clients.")]
    public List<GameObject> sharedObjects;

    [Tooltip("Array of GameObjects that will be made manipulatable by the MasterClient if the MasterClient is a Hololens.")]
    public List<GameObject> manipulatableObjects;


    private void Awake()
    {
        this.gameObject.tag = "SharedObjectsHolder";
    }

    private void Start ()
    {        
        TakeOwnership();
        ShowSharedObjects();
    }

    public void TakeOwnership()
    {
        #if UNITY_WSA
        if (PhotonNetwork.isMasterClient)
        {
            

            // Sets each GameObject in manipulatableObjects up to be manipulated by two handed input from the Hololens, but only if it's the MasterClient
            foreach (GameObject manipulatableObject in manipulatableObjects)
            {
                manipulatableObject.GetComponent<PhotonView>().RequestOwnership();
                var script = manipulatableObject.AddComponent<TwoHandManipulatable>();
                script.HostTransform = manipulatableObject.transform;
                script.ManipulationMode = ManipulationMode.MoveScaleAndRotate;
            }
        }
       #endif
    }


    public void ShowSharedObjects()
    {
        foreach (GameObject sharedObject in sharedObjects)
        {
            sharedObject.SetActive(true);
        }
    }

    public void HideSharedObjects()
    {
        foreach (GameObject sharedObject in sharedObjects)
        {
            sharedObject.SetActive(false);
        }
    }
	
}
