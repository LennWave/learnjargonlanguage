﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_WSA && !UNITY_EDITOR
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;
using UnityEngine.XR.WSA.Sharing;
#endif
public class WorldAnchorSharing : MonoBehaviour
{

#if UNITY_WSA && !UNITY_EDITOR
    WorldAnchorStore anchorStore;
    WorldAnchor mainAnchor_H;
#endif
    string anchorName = "MyAnchor";
    public Transform anchorObjectTransform;
    PhotonView photonView;
    private List<byte> exportDataList = new List<byte>();

    // Use this for initialization
#if UNITY_WSA && !UNITY_EDITOR
    void Start()
    {
        if (!this.GetComponent<WorldAnchor>())
        {
            mainAnchor_H = gameObject.AddComponent<WorldAnchor>(); // Add a World Anchor if there isn't one on the object
        }
        WorldAnchorStore.GetAsync(StoreLoaded);
        exportDataList.Clear();
    }

    private void StoreLoaded(WorldAnchorStore store)
    {
        this.anchorStore = store;
        mainAnchor_H = GetComponent<WorldAnchor>();
    }
#endif

    void LoadAnchor()
    {
#if UNITY_WSA && !UNITY_EDITOR
        bool loadTrue = anchorStore.Load(anchorName, this.gameObject);
        if (loadTrue)
        {
            anchorObjectTransform.position = transform.position;
            anchorObjectTransform.rotation = transform.rotation;
        }
#endif
    }

    void SaveAnchor()
    {
#if UNITY_WSA && !UNITY_EDITOR
        anchorStore.Delete(anchorName);
        if (!this.GetComponent<WorldAnchor>())
        {
            mainAnchor_H = gameObject.AddComponent<WorldAnchor>();
        }

        anchorStore.Save(anchorName, mainAnchor_H);
#endif
    }

    void ClearAnchor()
    {
#if UNITY_WSA && !UNITY_EDITOR
        if (this.GetComponent<WorldAnchor>())
        {
            DestroyImmediate(this.GetComponent<WorldAnchor>());
        }
#endif
    }

    public void SetWorldAnchor()
    {
        if (PhotonNetwork.isMasterClient) // Only the Host/MasterClient can set WorldAnchors
        {
            // Remove the WorldAnchor
            ClearAnchor();
            // Update the position/rotation of the Anchor Object
            transform.position = anchorObjectTransform.position;
            transform.rotation = anchorObjectTransform.rotation;
            //Save the WorldAnchor
            SaveAnchor();
        }
    }

    public void GetWorldAnchor()
    {
        if (PhotonNetwork.isMasterClient)
        {
            LoadAnchor(); //  If Host/MasterClient try to load a WorldAnchor from the store
            return;
        }
        RaiseEventOptions requestEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient };
        PhotonNetwork.RaiseEvent(1, null, true, requestEventOptions); // Send request for WorldAnchor to Host/MasterClient
    }



    private void OnEnable()
    {
#if UNITY_WSA && !UNITY_EDITOR
        PhotonNetwork.OnEventCall += OnEvent;
#endif

    }

    private void OnDisable()
    {
#if UNITY_WSA && !UNITY_EDITOR
        PhotonNetwork.OnEventCall -= OnEvent;
#endif
    }


    public void OnEvent(byte eventCode, object content, int senderID)
    {
#if UNITY_WSA && !UNITY_EDITOR
        /* [EVENTCODES]
         * eventCode 1 = Request Anchor Data from Host
         * eventCode 2 = Receive Anchor Data from Host
         * eventCode 3 = Exporting failed at some point at the Host
        */
        if (eventCode == 1)
        {
            ExportWorldAnchor();
            return;
        }
        if (eventCode == 2)
        {
            var importedDataArray = (byte[])content;
            ImportWorldAnchor(importedDataArray); // Try to import the byte[]
            return;
        }
        if(eventCode == 3)
        {
            RaiseEventOptions requestEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient };
            PhotonNetwork.RaiseEvent(1, null, true, requestEventOptions); // Request Host/MasterClient again for the WorldAnchor
        }


#endif
    }

#if UNITY_WSA && !UNITY_EDITOR
    private void ExportWorldAnchor()
    {
        //if we can succesfully load the Anchor then we can export it
        if (anchorStore.Load(anchorName, gameObject))
        {
            WorldAnchorTransferBatch transferBatch = new WorldAnchorTransferBatch();
            transferBatch.AddWorldAnchor(anchorName, mainAnchor_H);
            WorldAnchorTransferBatch.ExportAsync(transferBatch, OnExportDataAvailable, OnSerializationCompleted);
        }

    }

    void OnExportDataAvailable(byte[] data)
    {
        // Because the Serrialization is Async we first collect all the bytes in a list
        exportDataList.AddRange(data);
    }


    void OnSerializationCompleted(SerializationCompletionReason completionReason)
    {
        if (completionReason != SerializationCompletionReason.Succeeded) //If serialization failed for some reason then we clear the exportDataList for possible future use
        {
            exportDataList.Clear();
            RaiseEventOptions requestEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
            PhotonNetwork.RaiseEvent(3, null, true, requestEventOptions); // Inform Client(s) that Serialization failed
        }
        else // When succesfull send it over to the client
        {
            RaiseEventOptions requestEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
            PhotonNetwork.RaiseEvent(2, exportDataList.ToArray(), true, requestEventOptions); // Send the Client(s) the byte[] with the WorldAnchor
        }

    }



    private void ImportWorldAnchor(byte[] importedData)
    {
        WorldAnchorTransferBatch.ImportAsync(importedData, OnImportComplete);
    }


    private void OnImportComplete(SerializationCompletionReason completionReason, WorldAnchorTransferBatch deserializedTransferBatch)
    {
        //if (completionReason != SerializationCompletionReason.Succeeded)
        //{
        //    return;
        //}​

        string[] ids = deserializedTransferBatch.GetAllIds();
        foreach (string id in ids)
        {
            if (id == anchorName && gameObject != null)
            {
                deserializedTransferBatch.LockObject(id, gameObject); // Adds a WorldAnchor to the object that matches the WorldAnchor with id
                anchorObjectTransform.position = transform.position;
                anchorObjectTransform.rotation = transform.rotation;

            }
            else
            {
                Debug.Log("Failed to find object for anchor id: " + id);
            }
        }

    }
#endif

}
