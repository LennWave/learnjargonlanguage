﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_ANDROID 
    #if BreinWave_Aryzon
        using Aryzon ;
    #endif
    
    #if BreinWave_GoogleARCore
        using GoogleARCore;
    #endif
#endif

#if UNITY_WSA && BreinWave_Vuforia
    using Vuforia;
#endif

public class WorldCenter_Visualizer : MonoBehaviour {

    public Transform objectHolderTransform;

#if UNITY_ANDROID && BreinWave_GoogleARCore
    [Tooltip("ARCore Spatial Anchor")]
    public Anchor thisAnchor;
#endif


    // Use this for initialization
    void Awake ()
    {
#if UNITY_ANDROID
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //GameObject.FindGameObjectWithTag("Aryzon").GetComponent<AryzonTracking>().StartAryzonMode();
        return;
#endif
#if (UNITY_WSA || (UNITY_EDITOR && !UNITY_ANDROID)) && BreinWave_Vuforia
        VuforiaBehaviour.Instance.enabled = true;
#endif
    }

    private void OnEnable()
    {
        SetObjectHolderLocation();
    }

    private void OnDisable()
    {
        GameObject.FindGameObjectWithTag("SharedObjectsHolder").GetComponent<SharedObjectsHolderScript>().HideSharedObjects();
    }


    public void SetObjectHolderLocation()
    {
        var script = GameObject.FindGameObjectWithTag("SharedObjectsHolder").GetComponent<SharedObjectsHolderScript>();
        objectHolderTransform = script.transform;
        objectHolderTransform.position = this.transform.position;
        objectHolderTransform.rotation = this.transform.rotation;


#if UNITY_ANDROID && BreinWave_GoogleARCore
        if (thisAnchor != null)
        {
            objectHolderTransform.SetParent(thisAnchor.transform, true);
            objectHolderTransform.localPosition = Vector3.zero;
            objectHolderTransform.localRotation = Quaternion.identity;
        }
#endif

        script.ShowSharedObjects();
    }

    


}
