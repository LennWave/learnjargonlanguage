﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Breinwave.Editor
{
    [CustomEditor(typeof(FileDozerManager))]
    public class FileDozerManagerEditor : UnityEditor.Editor
    {

        //[MenuItem("BreinWave/Zip Manager &z")]
        static void Init()
        {
            var asset = AssetDatabase.LoadMainAssetAtPath("Assets/BreinWave/_General/Resources/FileDozerManager.asset");

            if (asset == null)
            {
                asset = ScriptableObject.CreateInstance<FileDozerManager>();

                AssetDatabase.CreateAsset(asset, "Assets/BreinWave/_General/Resources/FileDozerManager.asset");
                AssetDatabase.SaveAssets();
            }

            Selection.activeObject = asset;
        }

        private FileDozerManager Target
        {
            get
            {
                return (FileDozerManager)target;
            }
        }


        public override void OnInspectorGUI()
        {

            DrawProfiles();
            GUILayout.Space(5);
            AddProfile();
            GUILayout.Space(5);
            EditorUtility.SetDirty(target);
            serializedObject.ApplyModifiedProperties();
            serializedObject.UpdateIfRequiredOrScript(); //this is the actual apply changes

        }


        private void DrawProfiles()
        {


            for (int i = 0; i < Target.profiles.Count; i++)
            {
                FileDozerProfile zip = Target.profiles[i];
                GUI.color = Color.white;
                GUILayout.BeginVertical("Box");
                GUILayout.Label("Profile " + i.ToString());


                EditorGUILayout.BeginHorizontal();

                int index = i; // copy of index


                if (zip.inProjectPath.Count > 0)
                {

                    bool isInProject = zip.currentlyInProject;
                    string editorProfileText = isInProject ? "Move Files/Folders OUT of project." : "Move Files/Folders INTO project.";

                    if (GUILayout.Button(editorProfileText))
                    {
                        if (isInProject)
                        {
                            for (int j = 0; j < zip.inProjectPath.Count; j++)
                            { 


                                FileUtil.MoveFileOrDirectory(zip.inProjectPath[j], zip.outProjectPath[j]);
                                zip.currentlyInProject = false;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < zip.inProjectPath.Count; k++)
                            {
                                FileUtil.MoveFileOrDirectory(zip.outProjectPath[k], zip.inProjectPath[k]);
                                zip.currentlyInProject = true;
                            }
                        }

                        Repaint();
                    }
                }
                else
                {
                    GUILayout.Label("No File or Folder selected");
                }

                GUI.color = Color.red;
                // remove profile
                if (GUILayout.Button("Remove profile"))
                {

                    Target.profiles.Remove(Target.profiles[index]);

                    Repaint();

                    break;
                }
                GUI.color = Color.white;

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                if (GUILayout.Button("Add Folder"))
                {
                    var path = EditorUtility.SaveFolderPanel("Choose Folder", Application.dataPath, "");
                    if (zip.inProjectPath.Contains(path) || path == "")
                    {

                    }
                    else
                    {
                        AddDirectoryPaths(path, zip);
                    }
                }

                if (GUILayout.Button("Add File"))
                {
                    var path = EditorUtility.OpenFilePanel("Choose File", Application.dataPath,"");
                    if (zip.inProjectPath.Contains(path) || path == "")
                    {

                    }
                    else
                    {
                        AddDirectoryPaths(path, zip);
                    }
                }


                if (GUILayout.Button("Clear Directory Paths"))
                {
                    zip.inProjectPath.Clear();
                    zip.outProjectPath.Clear();
                }

                EditorGUILayout.EndHorizontal();

                SerializedProperty arrayElement = serializedObject.FindProperty("profiles").GetArrayElementAtIndex(i);
                EditorGUILayout.PropertyField(arrayElement, true);

                GUILayout.Label(" ");
                foreach (string path in zip.inProjectPath)
                {
                    GUILayout.Label(" ");
                }
                
                GUILayout.EndVertical();
            }
        }

        private void AddDirectoryPaths(string path, FileDozerProfile fdProfile)
        {
            string assetPath = "/Assets";
            string rootPath = Application.dataPath;
            rootPath = rootPath.Remove(rootPath.Length - assetPath.Length);

            int splitIndex = path.IndexOf(assetPath) + assetPath.Length;
            string tempPath = path.Substring(splitIndex);

            splitIndex = tempPath.LastIndexOf('/');
            string endOfPath = tempPath.Substring(splitIndex + 1);
            tempPath = tempPath.Remove(splitIndex);

            string[] directories = tempPath.Split('/');

            string growingPath = rootPath + "/FileDozerFiles";

            if (!Directory.Exists(growingPath))
            {
                Directory.CreateDirectory(growingPath);
            }

            foreach (string directory in directories)
            {
                if (!directory.Equals(""))
                {
                    growingPath += "/" + directory;
                    if (!Directory.Exists(growingPath))
                    {
                        Directory.CreateDirectory(growingPath);
                    }
                }

            }

            fdProfile.inProjectPath.Add(path);
            fdProfile.outProjectPath.Add(growingPath + "/" +endOfPath);
            Repaint();


        }

        private void AddProfile()
        {
            if (GUILayout.Button("Add profile"))
            {
                Target.profiles.Add(new FileDozerProfile());
                Target.profiles[Target.profiles.Count - 1].currentlyInProject = true;

                //Check if there is already a buildsuffix with "NONE"
                int suffixnr = 0;
                if (CheckForDefaultBuildSuffix(out suffixnr))
                {
                    Target.profiles[Target.profiles.Count - 1].BuildSuffix += "_" + suffixnr;
                }
                EditorUtility.SetDirty(Target);

                serializedObject.UpdateIfRequiredOrScript();
            }
        }

        bool CheckForDefaultBuildSuffix(out int freenr, int nr = 0)
        {
            string nrToCheck = "";
            bool nrfound = false;
            if (nr > 0)
            {
                nrToCheck = "_" + nr;
            }
            foreach (FileDozerProfile profile in Target.profiles)
            {
                if (profile.BuildSuffix == "NONE" + nrToCheck)
                {
                    nrfound = true;
                }
            }
            if (nrfound)
            {
                CheckForDefaultBuildSuffix(out freenr, nr + 1);
                return true;
            }
            freenr = nr;
            return false;
        }



    }
}
