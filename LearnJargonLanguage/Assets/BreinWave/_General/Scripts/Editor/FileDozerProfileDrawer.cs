﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BreinWave.Editor
{
    [CustomPropertyDrawer(typeof(FileDozerProfile))]
    public class FileDozerProfileDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            SerializedProperty pathListIn = property.FindPropertyRelative("inProjectPath");
            SerializedProperty pathListOut = property.FindPropertyRelative("outProjectPath");

            EditorGUI.indentLevel++;
            EditorGUI.PropertyField(position, pathListIn,true);
            EditorGUI.indentLevel--;

            EditorGUI.EndProperty();

        }
    }
}
