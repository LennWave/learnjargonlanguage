﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using BreinWave.Game;
#if UNITY_EDITOR
using UnityEditor;



namespace BreinWave.Editor
{
    /// <summary>
    /// Looks if certain namespaces are in the project and adds/removes the corresponding define symbols to PlayerSettings define symbols.
    /// To add your own define symbol use "BreinWave_" followed by the namespace name. Example: BreinWave_Holotoolkit
    /// </summary>
    public class SetDefineSymbols : UnityEditor.Editor
    {
        
       public static void UpdateDefineSymbols()
        {
            Game.XRProfileManager profileManager = AssetDatabase.LoadMainAssetAtPath("Assets/BreinWave/_General/Resources/XRProfileManager.asset") as Game.XRProfileManager;
            DefineSymbols valueToCheck = profileManager.CurrentProfile.m_defineSymbols;
            DefineSymbols temp = 0;

            int n = Enum.GetNames(typeof(DefineSymbols)).Length; //Number of options in the enum DefineSymbols 
            for(int i = 0; i <= n; i++)
            {

                int shortToCompare = (int)(1<<i);
                temp = (DefineSymbols)shortToCompare;
                string symbolName = temp.ToString();
                if (((int)valueToCheck & shortToCompare) == shortToCompare) //if a value is found then add it in the player settings.
                {
                    UpdateDefineSymbolsList(symbolName, true);
                }
                else // if a value is not found then remove it form the player settings.
                {
                    UpdateDefineSymbolsList(symbolName,false);
                }                
            }
            Debug.Log("Updated define symbols in the Player Settings.");
        }

        /// <summary>
        /// Update the current list of define symbols in the Player Settings.
        /// </summary>
        /// <param name="_symbol"></param> 
        /// <param name="adding">Bool to check if we want to add/remove _symbol to the define symbol list.</param>
        static void UpdateDefineSymbolsList(string _symbol, bool adding)
        {
            string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);

            List<string> allDefines = definesString.Split(';').ToList();

            if(adding)
            {
                if (!allDefines.Contains(_symbol))
                {
                    allDefines.Add(_symbol);
                }
            }
            else
            {
                if (allDefines.Contains(_symbol))
                {
                    allDefines.Remove(_symbol);
                }
            }                     

            PlayerSettings.SetScriptingDefineSymbolsForGroup(
            EditorUserBuildSettings.selectedBuildTargetGroup,
            string.Join(";", allDefines.ToArray()));
        }
               
    }
}
#endif