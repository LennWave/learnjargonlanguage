﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BreinWave.Game;
using BreinWave.Devices;
using System;

namespace BreinWave.Editor
{ 
    [CustomPropertyDrawer(typeof(XRProfile))]
    public class XRProfileDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUILayout.LabelField("Reality");

            SerializedProperty playerNameProp = property.FindPropertyRelative("PlayerName");

            SerializedProperty suffixProp = property.FindPropertyRelative("BuildSuffix");
            SerializedProperty deviceTypeProp = property.FindPropertyRelative("DeviceProfileType");
            SerializedProperty defineSymbolsProp = property.FindPropertyRelative("m_defineSymbols");
            SerializedProperty canHostProp = property.FindPropertyRelative("CanHost");
            SerializedProperty isCurrentprofileProp = property.FindPropertyRelative("isCurrentProfile");

            SerializedProperty scenesProp = property.FindPropertyRelative("ScenesPaths");

            SerializedProperty mobileTypeProp = property.FindPropertyRelative("MobileProfileType");
            SerializedProperty mobileOSProp = property.FindPropertyRelative("MobileProfileOS");

            SerializedProperty desktopTypeProp = property.FindPropertyRelative("DesktopProfileType");
            SerializedProperty desktopOSProp = property.FindPropertyRelative("DesktopProfileOS");

            EditorGUILayout.PropertyField(suffixProp);
            EditorGUILayout.PropertyField(playerNameProp);
            EditorGUILayout.PropertyField(deviceTypeProp);

            //DefineSymbols temp = (DefineSymbols)EditorGUILayout.EnumFlagsField((DefineSymbols)defineSymbolsProp.intValue);
            //defineSymbolsProp.intValue = (int)temp;
                    
            
            switch (deviceTypeProp.enumValueIndex)
            {
                case (int)XRDeviceType.Desktop:
                    EditorGUILayout.PropertyField(desktopTypeProp);
                    switch (desktopTypeProp.enumValueIndex)
                    {
                        case (int)XRDesktopType.DesktopApp:
                            EditorGUILayout.PropertyField(desktopOSProp);
                            break;

                        case (int)XRDesktopType.MixedRealityPortal:
                            desktopOSProp.enumValueIndex = (int)DesktopOS.Windows;
                            break;
                    }
                    break;

                case (int)XRDeviceType.Mobile:
                    EditorGUILayout.PropertyField(mobileTypeProp);
                    switch (mobileTypeProp.enumValueIndex)
                    {
                        case (int)XRMobileType.Aryzon:
                            EditorGUILayout.PropertyField(mobileOSProp);
                            break;

                        case (int)XRMobileType.MiraPrism6DOF:
                            mobileOSProp.enumValueIndex = (int)MobileOS.iOS;
                            break;

                        case (int)XRMobileType.MiraPrism3DOF:
                            mobileOSProp.enumValueIndex = (int)MobileOS.iOS;
                            break;

                        case (int)XRMobileType.VR:
                            EditorGUILayout.PropertyField(mobileOSProp);
                            break;
                    }
                    break;

                default:
                    break;
            }

            EditorGUILayout.PropertyField(canHostProp);
            if (!isCurrentprofileProp.boolValue)
            {
                CreateToggles(defineSymbolsProp);
            }
            else
            {
                EditorGUILayout.LabelField("Stop using this profile to edit define symbols.");
            }

            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(scenesProp, true);
            EditorGUI.indentLevel--;

            EditorGUI.EndProperty();

        }      

        void CreateToggles(SerializedProperty prop)
        {
            int toggleIntValue = 0;
            int enumLenght = prop.enumNames.Length;
            bool[] togglePressed = new bool[enumLenght];
            for (int i = 0; i < enumLenght; i++)
            {
                if((prop.intValue & (1 << i)) == 1<<i)
                {
                    togglePressed[i] = true;
                }
                togglePressed[i] = EditorGUILayout.Toggle( prop.enumNames[i], togglePressed[i],EditorStyles.toggle);

                if (togglePressed[i])
                    toggleIntValue += 1 << i;
            }


            prop.intValue = toggleIntValue;
            prop.serializedObject.ApplyModifiedProperties();
        }


        

    }


}