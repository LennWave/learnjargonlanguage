﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BreinWave.Game;
using System.IO;
#if BreinWave_HoloToolkit
using HoloToolkit.Unity;
#endif
using BreinWave.Devices;
using System.Diagnostics;
using Vuforia;
using Debug = UnityEngine.Debug;

namespace BreinWave.Editor
{   

    [CustomEditor(typeof(XRProfileManager))]
    public class XRProfileManagerEditor : UnityEditor.Editor
    {

        bool hasChanged = false;

        [MenuItem("BreinWave/XR device manager &x")]
        static void Init()
        {
            var asset = AssetDatabase.LoadMainAssetAtPath("Assets/BreinWave/_General/Resources/XRProfileManager.asset");

            if (asset == null)
            {
                asset = ScriptableObject.CreateInstance<XRProfileManager>();

                AssetDatabase.CreateAsset(asset, "Assets/BreinWave/_General/Resources/XRProfileManager.asset");
                AssetDatabase.SaveAssets();
            }

            Selection.activeObject = asset;
        }

        private XRProfileManager Target
        {
            get
            {
                return (XRProfileManager)target;
            }
        }

        private XRProfile buildProfile;

        public override void OnInspectorGUI()
        {

            StandardBuildPath();
            GUILayout.Space(5);
            DrawProfiles();
            GUILayout.Space(5);
            AddProfile();
            GUILayout.Space(5);
            BuildForAllProfiles();
            GUILayout.Space(5);

            //TestButton();

            EditorUtility.SetDirty(target);
            serializedObject.ApplyModifiedProperties();
            serializedObject.UpdateIfRequiredOrScript(); //this is the actual apply changes

        }

        private void StandardBuildPath()
        {
            GUILayout.BeginVertical("Box");
            GUILayout.Label("build path");
            GUILayout.BeginHorizontal();
            GUILayout.Label(Target.StandardBuildPath);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Change path"))
            {
                var path = EditorUtility.SaveFolderPanel("Choose Location of Game Built", Application.dataPath.Replace("/Assets", string.Empty), "");
                Target.StandardBuildPath = path;

            }
            if (GUILayout.Button("Open Explorer"))
            {
                if (File.Exists(Path.GetFullPath(Target.StandardBuildPath)))
                {
                    Process.Start("explorer.exe", "/select, " + Path.GetFullPath(Target.StandardBuildPath));
                }
                else
                {
                    Debug.LogError("No such path as " + Path.GetFullPath(Target.StandardBuildPath));

                    Debug.Log(Path.GetFullPath(Target.StandardBuildPath));
                    Debug.Log(Target.StandardBuildPath);

                }

            }
            if (GUILayout.Button("Clear"))
            {
                Target.StandardBuildPath = string.Empty;
            }

            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private string GetBuildPath()
        {
            if (string.IsNullOrEmpty(Target.StandardBuildPath))
            {
                return EditorUtility.SaveFolderPanel("Choose Save Location of Built", Application.dataPath.Replace("/Assets", string.Empty), "");
            }
            return Target.StandardBuildPath;
        }


        private void DrawProfiles()
        {            
            for (int i = 0; i < Target.profiles.Count; i++)
            {
                GUI.color = Color.white;
                bool trgtProfile = false;

                //compare on Build suffix, because in playmode, unity creates a copy of the profile
                if (Target.CurrentProfile != null)
                {
                    trgtProfile = Target.CurrentProfile.BuildSuffix == Target.profiles[i].BuildSuffix;
                }
                if (trgtProfile)
                {
                    //GUI.color = new Color(43f, 135f, 218f, 255f);
                    GUI.color = new Color(0, 1, 0.85f, 0.1f);//Cyan
                }

                GUILayout.BeginVertical("Box");
                GUI.color = Color.white;

                SerializedProperty arrayElement = serializedObject.FindProperty("profiles").GetArrayElementAtIndex(i);
                EditorGUILayout.PropertyField(arrayElement, true);

                EditorGUILayout.BeginHorizontal();

                SerializedProperty deviceTypeProp = arrayElement.FindPropertyRelative("deviceProfileType");

                int index = i; // copy of index

                if(trgtProfile)
                {
                    hasChanged = false;
                }

                string editorProfileText = trgtProfile ? "Stop using this profile" : "Use this profile in Editor";
                if (GUILayout.Button(editorProfileText))
                {
                    for (int j = 0; j < Target.profiles.Count; j++)
                    {
                        Target.profiles[j].isCurrentProfile = false;
                    }

                    if (!trgtProfile)
                    {
                        Target.profiles[i].isCurrentProfile = true;
                        Target.CurrentProfile = Target.profiles[i];
                        SwitchProfile(Target.profiles[i]);
                        SetDefineSymbols.UpdateDefineSymbols();
                    }
                    else
                    {
                        Target.CurrentProfile = null;
                    }
                    hasChanged = true;
                    Repaint();
                }


                if (trgtProfile)
                {

                    if (GUILayout.Button("Switch to Platform"))
                    {
                        SwitchPlatform(Target.profiles[i]);
                    }
                }

                //build button
                if (GUILayout.Button("Build for this device"))
                {
                    //delayed to catch InvalidOperationException: Operation is not valid due to the current state of the object System
                    // Debug.Log("Build profile " + deviceTypeProp.enumNames[deviceTypeProp.enumValueIndex] + " started");

                    buildProfile = Target.profiles[i];
                    EditorApplication.delayCall += BuildForProfile;
                }
                GUI.color = Color.red;

                // remove profile
                if (GUILayout.Button("Remove profile"))
                {

                    Target.profiles.Remove(Target.profiles[index]);

                    Repaint();
                    Debug.Log("Profile for " + deviceTypeProp.enumNames[deviceTypeProp.enumValueIndex] + " was removed");
                    break;
                }
                GUI.color = Color.white;

                EditorGUILayout.EndHorizontal();

                GUILayout.EndVertical();

                if(hasChanged)
                {
                    Repaint();
                }


            }
        }

        private void AddProfile()
        {
            if (GUILayout.Button("Add profile"))
            {
                Target.profiles.Add(new XRProfile());

                //Check if there is already a buildsuffix with "NONE"
                int suffixnr = 0;
                if (CheckForDefaultBuildSuffix(out suffixnr))
                {
                    Target.profiles[Target.profiles.Count - 1].BuildSuffix += "_" + suffixnr;
                }
                EditorUtility.SetDirty(Target);

                serializedObject.UpdateIfRequiredOrScript();
            }
        }

        private void BuildForAllProfiles()
        {
            if (GUILayout.Button("Build all profiles"))
            {
                foreach (var item in Target.profiles)
                {
                    BuildForProfile(item);
                }
            }
        }

        private void BuildForProfile()
        {
            if (buildProfile != null)
            {
                BuildForProfile(buildProfile);
            }
            else
            {
                Debug.LogError("No build profile was found");
            }
        }

        private void BuildForProfile(XRProfile profile)
        {
            //cannot build, becouse we have yet to code it.
            if (BuildPipeline.isBuildingPlayer)
            {
                Debug.LogError("Cannot build for " + profile.DeviceProfileType + ", Build already in progress");

            }
            else
            {
                //Set settings up correctly
                if (Target.CurrentProfile != profile)
                {
                    Target.CurrentProfile = profile;
                    SwitchProfile(profile);
                }

                //Set to correct Platform
                SwitchPlatform(profile);

                //create build options
                BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

#region Mobile Build Settings
                if (profile.DeviceProfileType == XRDeviceType.Mobile)
                {
                    //default building
                    string mobileExtention = ".none";
                    buildPlayerOptions.options = BuildOptions.None;
                    buildPlayerOptions.scenes = profile.ScenesPaths;

                    switch (profile.MobileProfileOS)
                    {
                        case MobileOS.None:
                            break;
                        case MobileOS.Android:

                            buildPlayerOptions.target = BuildTarget.Android;
                            buildPlayerOptions.targetGroup = BuildTargetGroup.Android;

                            mobileExtention = ".apk";

                            break;
                        case MobileOS.iOS:

                            buildPlayerOptions.target = BuildTarget.iOS;
                            buildPlayerOptions.targetGroup = BuildTargetGroup.iOS;

                            mobileExtention = ".ipa";

                            break;
                        default:

                            Debug.LogError("Unsupported value of 'target' in build profile");

                            break;
                    }

                    //set a path if no default
                    var path = GetBuildPath();
                    buildPlayerOptions.locationPathName = path + "/" + Application.productName + profile.BuildSuffix + mobileExtention;
                }
#endregion

#region Desktop Build Settings
                if (profile.DeviceProfileType == XRDeviceType.Desktop)
                {
                    //default building
                    string desktopExtention = ".none";
                    buildPlayerOptions.options = BuildOptions.None;
                    buildPlayerOptions.scenes = profile.ScenesPaths;

                    switch (profile.DesktopProfileOS)
                    {
                        case DesktopOS.None:
                            Debug.LogError("None is not a valid build target");
                            break;

                        case DesktopOS.Windows:

                            buildPlayerOptions.target = BuildTarget.StandaloneWindows;
                            buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;

                            desktopExtention = ".exe";

                            break;

                        case DesktopOS.OSX:

                            buildPlayerOptions.target = BuildTarget.StandaloneOSX;
                            buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;

                            desktopExtention = ".dmg";

                            break;

                        case DesktopOS.Linux:

                            buildPlayerOptions.target = BuildTarget.StandaloneLinux;
                            buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;

                            desktopExtention = ".sh";

                            break;

                        default:

                            Debug.LogError("Unsupported value of 'target' in build profile");

                            break;
                    }

                    //set a path if no default
                    var path = GetBuildPath();
                    buildPlayerOptions.locationPathName = path + "/" + Application.productName + profile.BuildSuffix + desktopExtention;
                }
#endregion

                switch (profile.DeviceProfileType)
                {
                    case XRDeviceType.Desktop:
                        BuildDesktop(profile);
                        break;
                    case XRDeviceType.Mobile:
                        BuildMobile(profile);
                        break;
                        #if BreinWave_HoloToolkit
                    case XRDeviceType.Hololens:
                        BuildHololens(profile);
                        break;
#endif
                    default:
                        Debug.LogError(string.Format("{0} is not supported.", profile.DeviceProfileType));
                        //cleanup and stop
                        Target.CurrentProfile = null;
                        return;
                }

                var result = string.Empty;

#region Building
                //building
                switch (profile.DeviceProfileType)
                {
                    case XRDeviceType.None:
                        break;

                    case XRDeviceType.Mobile:
                        break;

                    case XRDeviceType.Desktop:

                        //currently windows only
                        var path = GetBuildPath();
                        buildPlayerOptions.locationPathName = path + "/" + Application.productName + profile.BuildSuffix + ".exe";

                        break;

                    /*case XRDeviceType.Aryzon:
                        //options where set earlier according to MobileDeviceType
                        result = BuildPipeline.BuildPlayer(buildPlayerOptions);
                        break;*/
#if BreinWave_HoloToolkit
                    case XRDeviceType.Hololens:
                        // using the HoloToolkit BuildWindow (and methods)

                        //var bdw = new BuildDeployWindow();



                        //todo > pass build params

                        //bdw.BuildAll();

                        break;
#endif
                    /*case XRDeviceType.VR:

                        //options where set earlier according to MobileDeviceType
                        break;*/

                    default:
                        break;
                }
#endregion
#if BreinWave_HoloToolkit
                if (profile.DeviceProfileType != XRDeviceType.Hololens)
                {
                    result = BuildPipeline.BuildPlayer(buildPlayerOptions).ToString();
                }
#endif

                //result
                if (!string.IsNullOrEmpty(result))
                {
                    Debug.LogError(result);
                }
                else
                {
                    Debug.Log("Build saved at " + buildPlayerOptions.locationPathName);
                }

                //reset profile
                Target.CurrentProfile = null;
            }
        }

        public void TestButton()
        {
            if (GUILayout.Button("TEST"))
            {
                //var path = new FileInfo(@"C:\Users\LennartNeelis\Documents\unityprojects\CrossRealityAsset\Builds\VR\AndroidCrossRealityANDROID_VR.exe").DirectoryName;
                ////var path = Path.GetFullPath(");
                //DeleteDirectory(path);

            }
        }

        bool CheckForDefaultBuildSuffix(out int freenr, int nr = 0)
        {
            string nrToCheck = "";
            bool nrfound = false;
            if (nr > 0)
            {
                nrToCheck = "_" + nr;
            }
            foreach (XRProfile profile in Target.profiles)
            {
                if (profile.BuildSuffix == "NONE" + nrToCheck)
                {
                    nrfound = true;
                }
            }
            if (nrfound)
            {
                CheckForDefaultBuildSuffix(out freenr, nr + 1);
                return true;
            }
            freenr = nr;
            return false;
        }

#region BuildFunctions
        private string BuildDesktop(XRProfile profile)
        {
            string result = string.Empty;
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.scenes = profile.ScenesPaths;
            var path = GetBuildPath();


            switch (profile.DesktopProfileOS)
            {
                case DesktopOS.Windows:

                    buildPlayerOptions.target = BuildTarget.StandaloneWindows;
                    buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;

                    buildPlayerOptions.locationPathName = path + "/" + Application.productName + profile.BuildSuffix + ".exe";

                    switch (profile.DesktopProfileType)
                    {
                        case XRDesktopType.MixedRealityPortal:
                            //more buildsettings?
                            break;
                        case XRDesktopType.DesktopApp:
                            //more buildsettings?
                            break;
                        default:
                            Debug.LogError(string.Format("{0} is not supported for {1}.", profile.DesktopProfileType, profile.DesktopProfileOS));
                            result = "was not build";
                            return result;
                    }
                    break;
                case DesktopOS.OSX:

                    buildPlayerOptions.target = BuildTarget.StandaloneOSX;
                    buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;
                    buildPlayerOptions.locationPathName = path + "/" + Application.productName + profile.BuildSuffix + ".dmg";

                    switch (profile.DesktopProfileType)
                    {
                        case XRDesktopType.DesktopApp:

                            break;
                        default:
                            Debug.LogError(string.Format("{0} is not supported for {1}.", profile.DesktopProfileType, profile.DesktopProfileOS));
                            result = "error";
                            return result;
                    }

                    break;
                case DesktopOS.Linux:
                    switch (profile.DesktopProfileType)
                    {
                        case XRDesktopType.MixedRealityPortal:
                            //buildsettings. does linux support this?

                            break;
                        case XRDesktopType.DesktopApp:
                            //buildsettings. does linux support this?

                            break;
                        default:
                            Debug.LogError(string.Format("{0} is not supported for {1}.", profile.DesktopProfileType, profile.DesktopProfileOS));
                            result = "error";
                            return result;
                    }
                    break;
                default:
                    Debug.LogError(string.Format("{0} is not supported.", profile.DesktopProfileOS));

                    return result;
            }

            //if an error occured, this code may not be hit.

            result = BuildPipeline.BuildPlayer(buildPlayerOptions).ToString();

            return result;
        }

        private string BuildMobile(XRProfile profile)
        {
            string result = string.Empty;

            switch (profile.MobileProfileOS)
            {
                case MobileOS.Android:
                    switch (profile.MobileProfileType)
                    {
                        case XRMobileType.VR:
                            break;
                        //case XRMobileType.MiraPrism3DOF:
                        //    break;
                        //case XRMobileType.MiraPrism6DOF:
                        //    break;
                        case XRMobileType.Aryzon:
                            break;
                        default:
                            Debug.LogError(string.Format("{0} is not supported for {1}.", profile.MobileProfileType, profile.MobileProfileOS));
                            break;
                    }
                    break;
                case MobileOS.iOS:
                    switch (profile.MobileProfileType)
                    {
                        case XRMobileType.VR:

                            break;
                        case XRMobileType.MiraPrism3DOF:
                            break;
                        case XRMobileType.MiraPrism6DOF:
                            break;
                        case XRMobileType.Aryzon:

                            break;
                        default:
                            Debug.LogError(string.Format("{0} is not supported for {1}.", profile.MobileProfileType, profile.MobileProfileOS));
                            break;
                    }
                    break;
                default:
                    Debug.LogError(string.Format("{0} is not supported.", profile.MobileProfileOS));
                    break;
            }
            return result;

        }

        private string BuildHololens(XRProfile profile)
        {
            string result = string.Empty;


            return result;
        }
#endregion

        private void SwitchProfile(XRProfile profile)
        {
            switch (profile.DeviceProfileType)
            {
                case XRDeviceType.Desktop:

                    break;
                    #if BreinWave_HoloToolkit
                case XRDeviceType.Hololens:

                    break;
#endif
                case XRDeviceType.Mobile:
                    switch (profile.MobileProfileType)
                    {
                        case XRMobileType.Aryzon:
#if BreinWave_Vuforia

        Vuforia.EditorClasses.VuforiaConfigurationEditor.LoadConfigurationObject().DigitalEyewear.EyewearType = DigitalEyewearARController.EyewearType.None;
        Vuforia.EditorClasses.VuforiaConfigurationEditor.LoadConfigurationObject().Vuforia.DelayedInitialization = false;
#endif
#if BreinWave_Photn
                            SetAutoJoinOption(true);
#endif
                            break;
                    }
                    break;

                case XRDeviceType.None:
                    break;
            }
        }
#if BreinWave_Photn
        private void SetAutoJoinOption(bool automaticJoin)
        {
            try
            {
                FindObjectOfType<MultiplayerLobbyController>().AutomaticlyJoinGame = automaticJoin;
            }
            catch (NullReferenceException)
            {
                Debug.LogWarning("Menu Class not detected");
            }
        }
#endif

        private void SwitchPlatform(XRProfile profile)
        {

            switch (profile.DeviceProfileType)
            {
                case XRDeviceType.Desktop:
                    switch (profile.DesktopProfileType)
                    {
                        case XRDesktopType.DesktopApp:
                            switch (profile.DesktopProfileOS)
                            {
                                case DesktopOS.Linux:
                                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneLinuxUniversal);
                                    break;

                                case DesktopOS.OSX:
                                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX);
                                    break;

                                case DesktopOS.Windows:
                                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);
                                    break;
                            }

                            break;

                        case XRDesktopType.MixedRealityPortal:
                            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.WSA, BuildTarget.WSAPlayer);
                            EditorUserBuildSettings.wsaSubtarget = WSASubtarget.PC;
                            EditorUserBuildSettings.wsaBuildAndRunDeployTarget = WSABuildAndRunDeployTarget.LocalMachine;
                            break;
                    }

                    break;
#if BreinWave_HoloToolkit
                case XRDeviceType.Hololens:
                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.WSA, BuildTarget.WSAPlayer);
                    EditorUserBuildSettings.wsaSubtarget = WSASubtarget.HoloLens;
                    EditorUserBuildSettings.wsaBuildAndRunDeployTarget = WSABuildAndRunDeployTarget.LocalMachine;
                    break;
#endif
                case XRDeviceType.Mobile:
                    switch (profile.MobileProfileOS)
                    {
                        case MobileOS.Android:
                            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
                            break;

                        case MobileOS.iOS:
                            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS, BuildTarget.iOS);
                            break;
                    }
                    break;

                case XRDeviceType.None:
                    break;
            }
        }

        private void SetCustomDefine(XRProfile profile)
        {

        }
    }
}