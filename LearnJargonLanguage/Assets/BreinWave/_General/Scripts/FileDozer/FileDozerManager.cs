﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FileDozerManager : ScriptableObject
{

    [SerializeField]
    public List<FileDozerProfile> profiles = new List<FileDozerProfile>();

}
