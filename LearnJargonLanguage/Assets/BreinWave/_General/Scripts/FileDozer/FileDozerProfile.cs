﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FileDozerProfile
{
    public string BuildSuffix = "NONE";
    public bool currentlyInProject = true;
    public List<string> inProjectPath = new List<string>();
    public List<string> outProjectPath = new List<string>();

}
