﻿using BreinWave.Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR;

namespace BreinWave.Devices
{
    #region enums
    public enum XRDeviceType
    {
        None,
        Desktop,
        Mobile,
        Hololens
    }

    public enum XRMobileType
    {
        None,
        VR,
        MiraPrism3DOF,
        MiraPrism6DOF,
        Aryzon
    }

    public enum XRDesktopType
    {
        None,
        MixedRealityPortal,
        DesktopApp
    }

    public enum MobileOS
    {
        None,
        Android,
        iOS,
    }

    public enum DesktopOS
    {
        None,
        Windows,
        OSX,
        Linux
    }
#endregion

#region interfaces
    public interface IDevice
    {
        XRDeviceType GetDeviceType();

        GameObject GetVirtualDevice();

        void Initialize();
    }

    public interface IMobile
    {
        XRMobileType GetMobileType();
        MobileOS GetMobileOS();
    }

    public interface IDesktop
    {
        XRDesktopType GetDesktopType();
        DesktopOS GetDesktopOS();
    }
#endregion

#region abstract base classes
    public abstract class BaseDevice : IDevice
    {
        protected XRDeviceType type = XRDeviceType.None;

        internal GameObject _virtualDevice;

        public BaseDevice(XRProfile profile)
        {
            type = profile.DeviceProfileType;
        }

        public XRDeviceType GetDeviceType()
        {
            return type;
        }

        public abstract GameObject GetVirtualDevice();

        public virtual void Initialize()
        {
            GetVirtualDevice();
        }
    }

    public abstract class DesktopDevice : BaseDevice, IDesktop
    {
        private XRDesktopType desktopType = XRDesktopType.None;
        private DesktopOS os = DesktopOS.None;

        public override GameObject GetVirtualDevice()
        {
            if (_virtualDevice == null)
            {
                _virtualDevice = new GameObject();
                _virtualDevice.name = "DesktopDevice";
                _virtualDevice.AddComponent<Camera>();
                _virtualDevice.AddComponent<AudioListener>();
            }
            return _virtualDevice;
        }

        public DesktopDevice(XRProfile profile) : base(profile)
        {
            Debug.Log("Init desktop");
            
            //XRSettings.enabled = false;
            //VuforiaBehaviour.Instance.enabled = false;

            desktopType = profile.DesktopProfileType;
            os = profile.DesktopProfileOS;
        }

        public DesktopOS GetDesktopOS()
        {
            return os;
        }

        public XRDesktopType GetDesktopType()
        {
            return desktopType;
        }
    }

    public abstract class MobileDevice : BaseDevice, IMobile
    {
        private MobileOS os = MobileOS.None;
        private XRMobileType mobileType = XRMobileType.None;

        public MobileDevice(XRProfile profile) : base(profile)
        {
            os = profile.MobileProfileOS;
            mobileType = profile.MobileProfileType;

        }

        public  MobileOS GetMobileOS()
        {
            return os;
        }

        public XRMobileType GetMobileType()
        {
            return mobileType;
        }
    }
#endregion

#region Hololens Builds
    public class HololensDevice : BaseDevice
    {
        public HololensDevice(XRProfile profile) : base(profile)
        {
        }

        public override GameObject GetVirtualDevice()
        {
            // TODO!
            if (_virtualDevice == null)
            {

                GameObject cam = (GameObject)GameObject.Instantiate(Resources.Load("Camera/Cam_hololens"), Vector3.zero, Quaternion.identity);
                _virtualDevice = cam;  // new GameObject("Device Objects");
                GameObject input = (GameObject)GameObject.Instantiate(Resources.Load("Input/InputManager"), Vector3.zero, Quaternion.identity);
                GameObject cursor = (GameObject)GameObject.Instantiate(Resources.Load("Input/DefaultCursor"), Vector3.zero, Quaternion.identity);

                //cam.transform.SetParent(_virtualDevice.transform);
                input.transform.SetParent(_virtualDevice.transform);
                cursor.transform.SetParent(_virtualDevice.transform);
            }
            return _virtualDevice;
        }

    }
#endregion

#region Mobile Builds
    public class AryzonDevice : MobileDevice
    {
        public AryzonDevice(XRProfile profile) : base(profile)
        {

        }

        public override GameObject GetVirtualDevice()
        {
            if (_virtualDevice == null)
            {
#if BreinWave_GoogleARCore
                _virtualDevice = GameObject.Instantiate(Resources.Load("Camera/Cam_Aryzon_ARCore"), Vector3.zero, Quaternion.identity) as GameObject;
#else
                _virtualDevice = GameObject.Instantiate(Resources.Load("Camera/Cam_Aryzon.prefab"), Vector3.zero, Quaternion.identity) as GameObject;
#endif
            }
            return _virtualDevice;
        }

    }
    
    public class VRDevice : MobileDevice
    {
        public VRDevice(XRProfile profile) : base(profile)
        {
            Debug.Log("VRDevice");
        }

        public override GameObject GetVirtualDevice()
        {
            if (_virtualDevice == null)
            {
                _virtualDevice = GameObject.Instantiate(Resources.Load("Camera/Cam_VR"), Vector3.zero, Quaternion.identity) as GameObject;
            }
            return _virtualDevice;
        }
    }

    public class MiraPrism3DOF : MobileDevice
    {
        public MiraPrism3DOF(XRProfile profile) : base(profile)
        {
        }

        public override GameObject GetVirtualDevice()
        {
            throw new NotImplementedException();
        }
    }

    public class MiraPrism6DOF : MobileDevice
    {
        public MiraPrism6DOF(XRProfile profile) : base(profile)
        {
        }

        public override GameObject GetVirtualDevice()
        {
            throw new NotImplementedException();
        }
    }
#endregion

#region Desktop Builds
    public class MixedRealityPortal : DesktopDevice
    {
        public MixedRealityPortal(XRProfile profile) : base(profile)
        {
        }

        public override GameObject GetVirtualDevice()
        {
            if (_virtualDevice == null)
            {
                _virtualDevice = new GameObject("Device Objects");

                GameObject cam = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/CameraPrefabs/Samsung_VR", typeof(GameObject)), Vector3.zero, Quaternion.identity);

                cam.transform.SetParent(_virtualDevice.transform);

            }
            return _virtualDevice;
        }

        public override void Initialize()
        {
            GetVirtualDevice();
        }
    }

    public class DesktopApp : DesktopDevice
    {
        public DesktopApp(XRProfile profile) : base(profile)
        {
        }

        public override GameObject GetVirtualDevice()
        {
            //if (_virtualDevice == null)
            //    _virtualDevice = (GameObject)GameObject.Instantiate(Resources.Load("CameraPrefabs/Cam_desktop", typeof(GameObject)), Vector3.zero, Quaternion.identity);

            if (_virtualDevice == null)
            {
                _virtualDevice = new GameObject();
                _virtualDevice.name = "DesktopApp";
            }



            return _virtualDevice;
        }

        public override void Initialize()
        {
            GetVirtualDevice();
        }
    }
#endregion
}