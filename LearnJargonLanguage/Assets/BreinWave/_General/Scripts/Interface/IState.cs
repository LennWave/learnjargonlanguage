﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState {

    void OnComplete();
    void OnStart();
    void OnExit();
    void OnSuccess();
    void OnFail();
}
