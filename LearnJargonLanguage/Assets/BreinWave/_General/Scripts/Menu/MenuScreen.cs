﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreinWave.Menu
{
    public abstract class MenuScreen : MonoBehaviour
    {
        private void Start()
        {
//            Canvas canvas = GetComponentInParent<Canvas>();
//#if UNITY_WSA
            
//            RectTransform trans = canvas.gameObject.GetComponent<RectTransform>();
//            canvas.renderMode = RenderMode.WorldSpace; 
//            trans.position = new Vector3(0, 0, 3);
//            trans.localScale = new Vector3(0.001f, 0.001f, 0.001f);
//#else
//            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
//#endif


//#if UNITY_ANDROID && !VUFORIA
//            UnityEngine.XR.XRSettings.enabled = false;
//#endif
        }

        public abstract string Title { get; }

        public bool IsOpen
        {
            get
            {
                return this.gameObject.activeInHierarchy;
            }
        }

        public virtual void Open()
        {
            this.gameObject.SetActive(true);
        }

        public virtual void Close()
        {
            this.gameObject.SetActive(false);
        }

        public abstract MenuScreen GoBack();

    }
}