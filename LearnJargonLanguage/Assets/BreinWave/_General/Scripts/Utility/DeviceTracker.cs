﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreinWave.Singleton;
using BreinWave.Devices;
using BreinWave.Game;

public class DeviceTracker : Singleton<DeviceTracker> {

    public IDevice deviceInterface;

    string _deviceName = null;
    public string DeviceName
    {
        get
        {
            if(string.IsNullOrEmpty(_deviceName))
            {
                _deviceName = GameManager.Instance.CurrentProfile.PlayerName;
            }
            return _deviceName;
        }
    }

    public override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        //create and keep the player
        var deviceType = GameManager.Instance.CurrentDevice.GetDeviceType();

        deviceInterface = GameManager.Instance.CurrentDevice;

        deviceInterface.GetVirtualDevice().transform.SetParent(this.transform);

        deviceInterface.Initialize();

    }
}
