﻿using BreinWave.Devices;
using BreinWave.Singleton;
using UnityEngine;

#if BreinWave_HoloToolkit
#endif

namespace BreinWave.Game
{
    public class GameManager : Singleton<GameManager>
    {
        private XRProfile _currentProfile;
        public XRProfile CurrentProfile
        {
            get
            {
                if (_currentProfile == null)
                {
                    _currentProfile = GetProfile();
                }
                return _currentProfile;
            }
        }


        private IDevice _currentDevice;
        public IDevice CurrentDevice
        {
            get
            {
                if (_currentDevice == null)
                {
                    _currentDevice = GetDevice();
                }
                return _currentDevice;
            }
        }

        public override void Awake()
        {
            base.Awake();

            if (!isDestroyed)
            {
                DontDestroyOnLoad(this.gameObject);
                CurrentDevice.GetVirtualDevice().transform.SetParent(this.transform);
                CurrentDevice.Initialize();
            }
        }

        private XRProfile GetProfile()
        {
            var asset = Resources.Load("XRProfileManager") as XRProfileManager;
            if (asset == null)
            {
                Debug.LogError("XR Profile Manager resource not found.");
                return null;
            }

            Debug.Log(asset.CurrentProfile.DeviceProfileType + " profile type was loaded. (AKA:" + asset.CurrentProfile.PlayerName + ")");

            return asset.CurrentProfile;
        }

        private IDevice GetDevice()
        {
            switch (CurrentProfile.DeviceProfileType)
            {
                case XRDeviceType.None:
                    // probably editor mode
                    break;

                case XRDeviceType.Desktop:
                    switch (CurrentProfile.DesktopProfileType)
                    {
                        case XRDesktopType.None:
                            break;

                        case XRDesktopType.DesktopApp:
                            return new DesktopApp(CurrentProfile);

                        case XRDesktopType.MixedRealityPortal:
                            return new MixedRealityPortal(CurrentProfile);
                    }
                    break;

                case XRDeviceType.Mobile:
                    switch (CurrentProfile.MobileProfileType)
                    {
                        case XRMobileType.None:
                            break;

                        case XRMobileType.Aryzon:
                            return new AryzonDevice(CurrentProfile);

                        case XRMobileType.VR:
                            return new VRDevice(CurrentProfile);

                        case XRMobileType.MiraPrism3DOF:
                            return new MiraPrism3DOF(CurrentProfile);

                        case XRMobileType.MiraPrism6DOF:
                            return new MiraPrism6DOF(CurrentProfile);
                    }
                    break;
#if BreinWave_HoloToolkit
                case XRDeviceType.Hololens:
                    return new HololensDevice(CurrentProfile);
#endif
                default:
                    break;
            }

            return null;
        }
    }
}