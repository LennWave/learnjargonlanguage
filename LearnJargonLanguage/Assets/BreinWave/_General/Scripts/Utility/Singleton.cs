﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreinWave.Singleton
{
    public class Singleton<T> : MonoBehaviour
        where T : MonoBehaviour
    {
        protected bool isDestroyed = false;

        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = CreateInstance();
                }
                return _instance;
            }
        }

        private static T CreateInstance()
        {
            Debug.Log("Creating singleton of type " + typeof(T).Name);

            var obj = new GameObject();
            obj.name = typeof(T).Name;

            return obj.AddComponent<T>();
        }

        public virtual void Awake()
        {
            if (_instance != null)
            {
                Debug.Log(typeof(T).Name + " Instance Destroyed ");
                isDestroyed = true;
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this as T;
            }
        }
    }
}