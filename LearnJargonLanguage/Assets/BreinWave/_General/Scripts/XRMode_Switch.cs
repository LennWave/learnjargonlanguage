﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class XRMode_Switch : MonoBehaviour {


    static Coroutine modeSwitchRoutine = null;
    static XRMode_Switch instance;

    public void Awake()
    {
        instance = this;

#if BreinWave_VR
        SwitchToVR();
#endif
    }

    public static void SwitchToVR()
    {        
        if(modeSwitchRoutine == null)
        {
            modeSwitchRoutine = instance.StartCoroutine(instance.SwitchToVRCoroutine());
        }
    }

    public static void SwitchXROff()
    {
        
        if (modeSwitchRoutine == null)
        {
            modeSwitchRoutine = instance.StartCoroutine(instance.SwitchXROffCoroutine());
        }
    }
    
    IEnumerator SwitchXROffCoroutine()
    {
        XRSettings.LoadDeviceByName("");
        yield return null;
        XRSettings.enabled = false;
        modeSwitchRoutine = null;
    }


    IEnumerator SwitchToVRCoroutine()
    {
        XRSettings.LoadDeviceByName("Cardboard");
        yield return null;
        XRSettings.enabled = true;
        modeSwitchRoutine = null;      
    }
    





}
