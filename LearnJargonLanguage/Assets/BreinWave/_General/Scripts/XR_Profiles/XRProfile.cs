﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreinWave.Devices;

namespace BreinWave.Game
{
    [System.Flags]
    public enum DefineSymbols : int
    {                       
        BreinWave_Aryzon        = 1 << 0,          
        BreinWave_GoogleARCore  = 1 << 1,  
        BreinWave_HoloToolkit   = 1 << 2,     
        BreinWave_Photon        = 1 << 3,          
        BreinWave_VR            = 1 << 4,             
        BreinWave_Vuforia       = 1 << 5,

        /*  
        If you wish to add new symbols you do it as follows:
        BreinWave_NewSymbolName1    = 1 << 6,
        BreinWave_NewSymbolName2    = 1 << 7,
        Etc.
        */

    }

    [Serializable]
    public class XRProfile
    {
        public string BuildSuffix = "NONE";

        public string PlayerName = "Player";
        
        public XRDeviceType DeviceProfileType = XRDeviceType.None;

        /// <summary>
        /// Type of Mobile build you want to make. Only use if build is for mobile
        /// </summary>
        public XRMobileType MobileProfileType = XRMobileType.None;

        /// <summary>
        /// The Operation System of the mobile it runs on. Only use if build is for mobile
        /// </summary>
        public MobileOS MobileProfileOS = MobileOS.None;

        /// <summary>
        /// Type of Desktop build you want to make. Only use is build is for desktop
        /// </summary>
        public XRDesktopType DesktopProfileType = XRDesktopType.None;

        /// <summary>
        /// The Operation System of the desktop it runs on. Only use if build is for dekstop
        /// </summary>
        public DesktopOS DesktopProfileOS = DesktopOS.None;

        public DefineSymbols m_defineSymbols = 0;

        public bool CanHost = false;

        public string[] ScenesPaths;

        public bool isCurrentProfile = false;

    }
}