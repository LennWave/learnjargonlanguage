﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreinWave.Devices;
using BreinWave.Game;
using UnityEngine.SceneManagement;

namespace BreinWave.Game
{
    [System.Serializable]
    public class XRProfileManager : ScriptableObject
    {
        //move this to editorprefs
        public string StandardBuildPath;

        [SerializeField]
        public XRProfile CurrentProfile;

        [SerializeField]
        public List<XRProfile> profiles = new List<XRProfile>();
    }
}