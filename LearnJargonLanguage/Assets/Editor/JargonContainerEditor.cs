﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;



[CustomEditor(typeof(JargonContainer))]
public class JargonContainerEditor : Editor
{
    private JargonLanguageSet def;
    private JargonLanguageSet DefaultLanguage
    {
        get
        {
            if (def == null)
                def = LanguageSetEditor.DefaultLanguageSet;
            return def;
        }
    }

    private JargonContainer Target
    {
        get
        {
            return (JargonContainer)target;
        }
    }

    private List<JargonMappingDrawer> drawers = new List<JargonMappingDrawer>();
    private List<GameObject> mappingObjects = new List<GameObject>();


    public GameObject Preview;
    GameObject PreviewedModel;

    [MenuItem("BreinWave/Create JargonContainer")]
    static void Init()
    {

        var count = Resources.FindObjectsOfTypeAll(typeof(JargonContainer)).Length;
        Object asset = null;

        Debug.Log("Named asset " + count + ". has a value of " + asset);

        for (int i = count; i < count + 1; i++)
        {
            count = i;
            asset = AssetDatabase.LoadMainAssetAtPath("Assets/PhotonResources/Container/Resources/JargonContainer_" + count + ".asset");
            if (asset == null)
            {
                break;
            }
        }

        asset = ScriptableObject.CreateInstance<JargonContainer>();

        AssetDatabase.CreateAsset(asset, "Assets/PhotonResources/Container/Resources/JargonContainer_" + count + ".asset");
        AssetDatabase.SaveAssets();
    }

    private void OnDisable()
    {

        SaveMarkerPositions();
    }

    private void OnEnable()
    {
        Preview = GameObject.Find("Preview");
        if (Preview != null)
        {
            var mymodel = Preview.transform.Find(Target.ModelName);

            //slected same data as before
            if (mymodel != null)
            {
                //Awake();
                PreviewedModel = mymodel.gameObject;
                mappingObjects = new List<GameObject>();

                //map the (existing) objects
                for (int i = 0; i < Target.Mapping.Count; i++)
                {
                    var mapping = Target.Mapping[i];
                    if (string.IsNullOrEmpty(mapping.CommonID))
                    {
                        mappingObjects.Add(null);
                        continue;
                    }
                    var term = DefaultLanguage.Terms.Where(x => x.CommonID == mapping.CommonID).FirstOrDefault();
                    if (term == null)
                    {
                        mappingObjects.Add(null);
                        continue;
                    }
                    var found = Preview.transform.Find(term.Name + mapping.UniqueId);
                    if (found != null)
                    {
                        mappingObjects.Add(found.gameObject);
                    }
                }
            }
            else
            {
                RemovePreview();
                ShowPreview();
            }

            SaveMarkerPositions();

        }
    }

    private void SaveMarkerPositions()
    {
        JargonContainer[] containers = Resources.FindObjectsOfTypeAll(typeof(JargonContainer))as JargonContainer[];
        string viewTarget = EditorPrefs.GetString("PreviewContainer",string.Empty);
        JargonContainer saveTarget = null;

        if (!string.IsNullOrEmpty(viewTarget))
        {
            //Debug.Log("possible targets = " + containers.Length);
            saveTarget = containers.Where(x => x.name == viewTarget).FirstOrDefault();
            //Debug.Log("Found target = " + saveTarget);
        }

        if (saveTarget == null || Preview == null)
        {
            if (saveTarget == null)
                Debug.LogWarning("No Target  " + saveTarget);
            if (Preview == null)
                Debug.LogWarning("No Preview " + Preview);

            return;
        }

        Debug.Log("Save Marker Positions for " + saveTarget.name);

        for (int i = 0; i < saveTarget.Mapping.Count; i++)
        {
            var mapping = saveTarget.Mapping[i];
            if (string.IsNullOrEmpty(mapping.CommonID))
                continue;

            var term = DefaultLanguage.Terms.Where(x => x.CommonID == mapping.CommonID).FirstOrDefault();
            if (term == null)
                continue;

            var mappingObj = mappingObjects[i];
            if (mappingObj != null)
            {
                //if the locations differ
                if (mapping.Location != mappingObj.transform.position)
                {
                    mapping.Location = mappingObj.transform.position;
                }
            }
        }


    }



    private void Awake()
    {
        drawers = new List<JargonMappingDrawer>();
        for (int i = 0; i < Target.Mapping.Count; i++)
        {
            drawers.Add(new JargonMappingDrawer());
        }
    }

    public void ShowPreview()
    {
        if (Preview != null)
            RemovePreview();

        Debug.Log("Creating preview");

        //create container.
        Preview = new GameObject("Preview");
        Preview.transform.position = Vector3.zero;

        //create model
        PreviewedModel = Instantiate(Target.Model, Vector3.zero, Quaternion.identity, Preview.transform) as GameObject;
        PreviewedModel.name = Target.ModelName;

        //save selected model name
        EditorPrefs.SetString("PreviewContainer", target.name);

        //add mapping
        //var mappingPrefab = Resources.Load<GameObject>("Mapping");
        mappingObjects = new List<GameObject>();

        for (int i = 0; i < Target.Mapping.Count; i++)
        {
            var mapping = Target.Mapping[i];
            if (mapping == null)
            {
                Debug.LogError("Mapping is null");
                continue;
            }

            var term = DefaultLanguage.Terms.Where(x => x.CommonID == mapping.CommonID).FirstOrDefault();
            if (term == null)
            {
                Debug.LogError("no term found for id " + mapping.CommonID);
                mappingObjects.Add(null);
                continue;
            }

            GameObject map = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            map.GetComponent<MeshRenderer>().sharedMaterial.color = Color.blue;

            map.transform.SetParent(Preview.transform);
            map.name = term.Name + mapping.UniqueId;
            map.transform.localPosition = mapping.Location;
            mappingObjects.Add(map);
            drawers[i].realWorldObject = map;
        }

        //for (int i = 0; i < Target.Mapping.Count; i++)
        //{
        //    var mapping = Target.Mapping[i];
        //    GameObject map = Instantiate(mappingPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        //    map.transform.SetParent(Preview.transform);
        //    var term = DefaultLanguage.Terms.Where(x => x.CommonID == mapping.CommonID).FirstOrDefault();
        //    if (term != null)
        //    {
        //        map.transform.Find("Sphere").transform.Find("Title").GetComponent<TextMesh>().text = term.Name;
        //        map.transform.Find("Sphere").name = mapping.CommonID;
        //        map.name = term.Name;
        //        map.transform.localPosition = mapping.Location;
        //    }
        //}
        //add everthing the button would do.
    }

    public void RemovePreview()
    {
        DestroyImmediate(Preview);
        mappingObjects = new List<GameObject>();
    }

    public override void OnInspectorGUI()
    {
        if (drawers.Count != Target.Mapping.Count)
        {
            Awake();
        }

        DrawPreview();
        GUILayout.Space(5);
        DrawDefaultValues();
        GUILayout.Space(5);
        DrawMapping();
        GUILayout.Space(5);

        EditorUtility.SetDirty(Target);
        serializedObject.ApplyModifiedProperties();
        serializedObject.UpdateIfRequiredOrScript(); //this is the actual apply changes
    }

    public void DrawDefaultValues()
    {
        Target.ModelName = EditorGUILayout.TextField("Name", Target.ModelName);
        Target.Model = EditorGUILayout.ObjectField("Model", Target.Model, typeof(GameObject), false) as GameObject;

    }


    public void DrawPreview()
    {
        if (Preview == null)
        {
            if (GUILayout.Button("Preview Object"))
            {
                ShowPreview();
            }
        }
        else
        {
            if (GUILayout.Button("Cancel Preview"))
            {
                SaveMarkerPositions();
                RemovePreview();
                return;
            }
        }
    }

    public void DrawMapping()
    {
        bool hasPreview = Target.Mapping.Count == mappingObjects.Count;
        for (int i = 0; i < Target.Mapping.Count; i++)
        {
            //begin box 1
            GUI.backgroundColor = Color.grey;
            EditorGUILayout.BeginVertical("Box");
            GUI.backgroundColor = Color.white;

            SerializedProperty arrayElement = serializedObject.FindProperty("Mapping").GetArrayElementAtIndex(i);

            drawers[i].OnGUI(Rect.zero, arrayElement, null);

            if (hasPreview)
            {
                drawers[i].realWorldObject = mappingObjects[i];
            }


            EditorGUILayout.BeginHorizontal("Box");

            GUI.color = Color.red;
            if (GUILayout.Button("Remove"))
            {
                Target.Mapping.RemoveAt(i);
                drawers.RemoveAt(i);


                if (Preview != null)
                {
                    DestroyImmediate(mappingObjects[i]);
                    mappingObjects.RemoveAt(i);

                    SaveMarkerPositions();
                    ShowPreview();
                }
            }
            GUI.color = Color.white;

            EditorGUILayout.EndHorizontal();

            //end box 1
            EditorGUILayout.EndVertical();
        }


        GUI.color = Color.cyan;
        if (GUILayout.Button("Add mapping"))
        {
            var map = new JargonMapping();
            Target.Mapping.Add(map);

            var drawer = new JargonMappingDrawer();
            drawers.Add(drawer);

            if (Preview)
                mappingObjects.Add(null);


            EditorUtility.SetDirty(Target);
            serializedObject.UpdateIfRequiredOrScript();
        }
        GUI.color = Color.white;


    }
}
