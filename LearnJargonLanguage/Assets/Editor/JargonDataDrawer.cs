﻿using UnityEditor;
using UnityEngine;


[CustomPropertyDrawer(typeof(JargonData))]
public class JargonDataDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        EditorGUILayout.BeginVertical("Box");

        //SerializedProperty commonid = property.FindPropertyRelative("CommonID");
        //EditorGUILayout.LabelField("CommonId",commonid.stringValue);
        
        SerializedProperty nameprop = property.FindPropertyRelative("Name");
        EditorGUILayout.PropertyField(nameprop);

        SerializedProperty descriptionprop = property.FindPropertyRelative("Description");

        GUILayout.Label("Description");
        descriptionprop.stringValue = EditorGUILayout.TextArea(descriptionprop.stringValue);

        EditorGUILayout.EndVertical();

        EditorGUI.EndProperty();

    }

}
