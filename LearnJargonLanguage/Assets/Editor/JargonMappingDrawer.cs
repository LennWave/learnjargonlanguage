﻿using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(JargonMapping))]
public class JargonMappingDrawer : PropertyDrawer
{
    string search = "search here";

    private JargonLanguageSet def;

    public bool selectTerm = false;

    public GameObject realWorldObject;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(Rect.zero, label, property);
        GUILayout.BeginVertical();

       

    SerializedProperty commonid = property.FindPropertyRelative("CommonID");

        //find english name from the common id

        if (def == null)
            def = LanguageSetEditor.DefaultLanguageSet;

        var term =  def.Terms.Where(x => x.CommonID == commonid.stringValue).FirstOrDefault();

        EditorGUILayout.BeginVertical("Box");
        if (term != null)
        {

            //EditorGUILayout.LabelField("CommonId", term.CommonID);
            EditorGUILayout.LabelField("Term", term.Name);


            SerializedProperty locationX = property.FindPropertyRelative("PosX");
            //EditorGUILayout.PropertyField(locationX);
            SerializedProperty locationY = property.FindPropertyRelative("PosY");
            //EditorGUILayout.PropertyField(locationY);
            SerializedProperty locationZ = property.FindPropertyRelative("PosZ");
            //EditorGUILayout.PropertyField(locationZ);

            Vector3 loca = EditorGUILayout.Vector3Field("Location", new Vector3(locationX.floatValue, locationY.floatValue, locationZ.floatValue));

            if(realWorldObject != null)
            {
                //position changed
                if(loca != new Vector3(locationX.floatValue, locationY.floatValue, locationZ.floatValue))
                {
                    //Debug.Log("editor changed");
                    realWorldObject.transform.position = loca;
                }
                else if(realWorldObject.transform.position != loca)
                {
                    loca = realWorldObject.transform.position;
                    //Debug.Log("world object changed");

                }
            }

            locationX.floatValue = loca.x;
            locationY.floatValue = loca.y;
            locationZ.floatValue = loca.z;

            if (GUILayout.Button("Change term"))
            {
                commonid.stringValue = string.Empty;
                selectTerm = true;
            }

            //EditorGUILayout.LabelField("Description", term.Description);
        }
        else
        {
            GUI.color = Color.yellow;
            EditorGUILayout.BeginVertical("Box");
            GUI.color = Color.white;

            if (selectTerm)
            {
                GUI.color = Color.green;
                if (GUILayout.Button("Collapse ^"))
                {
                    selectTerm = false;
                }
                GUI.color = Color.white;


                EditorGUILayout.BeginVertical("Box");

                foreach (var item in def.Terms)
                {
                    if (GUILayout.Button(item.Name))
                    {
                        commonid.stringValue = item.CommonID;
                        selectTerm = false;
                    }
                }

                EditorGUILayout.EndVertical();
            }
            else
            {
                GUI.color = Color.green;
                if (GUILayout.Button("Choose a term"))
                {
                    selectTerm = true;
                }
                GUI.color = Color.white;

            }


            EditorGUILayout.EndVertical();
            //GUI.color = Color.white;

            //var style = new GUIStyle(GUI.skin.textArea);
            //search = EditorGUILayout.TextField(search, search, style);

        }

        EditorGUILayout.EndVertical();



        GUILayout.EndVertical();

        EditorGUI.EndProperty();


    }

}

