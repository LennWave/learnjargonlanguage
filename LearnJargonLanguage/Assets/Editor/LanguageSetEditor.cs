﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(JargonLanguageSet))]
public class LanguageSetEditor : Editor
{
    private JargonLanguageSet Target
    {
        get
        {
            return (JargonLanguageSet)target;
        }
    }

    private static JargonLanguageSet _defaultLanguageSet;
    public static JargonLanguageSet DefaultLanguageSet
    {
        get
        {
            //nicley hardcoded. :D
            if (_defaultLanguageSet == null)
                _defaultLanguageSet = AssetDatabase.LoadMainAssetAtPath("Assets/PhotonResources/LanguageDatabase/Resources/English.asset") as JargonLanguageSet;
            return _defaultLanguageSet;
        }
    }



    [MenuItem("BreinWave/Create Language set")]
    static void Init()
    {
        var asset = ScriptableObject.CreateInstance<JargonLanguageSet>();

        AssetDatabase.CreateAsset(asset, "Assets/PhotonResources/LanguageDatabase/Resources/MyNewLanguageSet.asset");
        AssetDatabase.SaveAssets();
    }

    private void Awake()
    {
        DefaultLanguageSet.Terms = DefaultLanguageSet.Terms.OrderBy(x => x.Name).ToList();
        Target.Terms = Target.Terms.OrderBy(x => x.Name).ToList();

        //remove doubles
        List<string> ids = new List<string>();
        foreach (var t in Target.Terms)
        {
            if (ids.Contains(t.CommonID))
            {
                Target.Terms.Remove(t);
            }
            else
            {
                ids.Add(t.CommonID);
            }
        }

    }

    public override void OnInspectorGUI()
    {
        DrawDefaultValues();
        GUILayout.Space(5);
        DrawData();

        serializedObject.ApplyModifiedProperties();
        serializedObject.UpdateIfRequiredOrScript(); //this is the actual apply changes

    }

    public GUIStyle GetStyle()
    {
        if (GUI.skin.customStyles.Length > 0)
        {
            GUI.skin.customStyles[0].richText = true;
            GUI.skin.customStyles[0].normal.textColor = Color.white;
            GUI.skin.customStyles[0].onNormal.textColor = Color.white;

            //GUI.skin.customStyles[0]..textColor = Color.white;


            return GUI.skin.customStyles[0];
        }
        else
        {
            GUIStyle richStyle = new GUIStyle(GUI.skin.label);

            richStyle.richText = true;
            richStyle.normal.textColor = Color.white;
            richStyle.onNormal.textColor = Color.white;

            return richStyle;
        }
    }

    void DrawDefaultValues()
    {
        Target.LanguageName = EditorGUILayout.TextField("Language name: ", Target.LanguageName);
    }


    public void DrawData()
    {
        GUIStyle MyStyle = GetStyle();

        if (DefaultLanguageSet != null && Target != DefaultLanguageSet)
        {
            for (int i = 0; i < DefaultLanguageSet.Terms.Count; i++)
            {
                var commonid = DefaultLanguageSet.Terms[i].CommonID;
                var targetTerm =  Target.Terms.Where(x => x.CommonID == commonid).FirstOrDefault();
                var index = 0;

                //containter. box 1
                GUI.backgroundColor = (targetTerm == null ? Color.red : Color.cyan);
                EditorGUILayout.BeginVertical("Box");

                GUILayout.Label(DefaultLanguageSet.LanguageName);

                //box 2
                GUI.backgroundColor = Color.gray;
                EditorGUILayout.BeginVertical("Box");
                GUI.backgroundColor = Color.white;

                //EditorGUILayout.LabelField("CommonId", commonid, MyStyle);
                EditorGUILayout.LabelField("Name", DefaultLanguageSet.Terms[i].Name, MyStyle);
                EditorGUILayout.LabelField("Description", DefaultLanguageSet.Terms[i].Description, MyStyle);

                //end box 2
                EditorGUILayout.EndVertical();

                // no term in this language?
                if (targetTerm == null)
                {
                    if (GUILayout.Button("Add translation"))
                    {
                        targetTerm = new JargonData(DefaultLanguageSet.Terms[i]);
                        Target.Terms.Add(targetTerm);

                        EditorUtility.SetDirty(Target);
                        serializedObject.UpdateIfRequiredOrScript();
                        return;
                    }
                }
                else
                {
                    index = Target.Terms.IndexOf(targetTerm);

                    //use the property drawer
                    GUILayout.Label(Target.LanguageName + " translation");

                    SerializedProperty arrayElement = serializedObject.FindProperty("Terms").GetArrayElementAtIndex(index);
                    new JargonDataDrawer().OnGUI(Rect.zero, arrayElement, null);
                }


                //end box 1
                EditorGUILayout.EndVertical();

            }
        }
        else if (Target == DefaultLanguageSet)
        {
            //this set is the english language. (the default)
            if (GUILayout.Button("Add term"))
            {
                Target.Terms.Add(new JargonData(GUID.Generate().ToString()));
                EditorUtility.SetDirty(Target);
                serializedObject.UpdateIfRequiredOrScript();
            }

            EditorGUILayout.BeginVertical();
            for (int i = 0; i < Target.Terms.Count; i++)
            {
                SerializedProperty arrayElement = serializedObject.FindProperty("Terms").GetArrayElementAtIndex(i);
                EditorGUILayout.PropertyField(arrayElement, true);
            }
            EditorGUILayout.EndVertical();
        }
        else
        {
            Debug.LogError("There must be a default language");
        }
    }
}
