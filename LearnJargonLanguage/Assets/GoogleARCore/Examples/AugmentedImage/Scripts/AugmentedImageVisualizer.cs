//-----------------------------------------------------------------------
// <copyright file="AugmentedImageVisualizer.cs" company="Google">
//
// Copyright 2018 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.Examples.AugmentedImage
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using Aryzon;
    using GoogleARCore;
    using GoogleARCore.Examples.HelloAR;
    using GoogleARCoreInternal;
    using UnityEngine;

    /// <summary>
    /// Uses 4 frame corner objects to visualize an AugmentedImage.
    /// </summary>
    public class AugmentedImageVisualizer : MonoBehaviour
    {
        public GameObject centerObjectPrefab;
        public Anchor thisAnchor;

        /// <summary>
        /// The AugmentedImage to visualize.
        /// </summary>
        public AugmentedImage Image;

        /// <summary>
        /// A model for the lower left corner of the frame to place when an image is detected.
        /// </summary>
        public Canvas buttonCanvas;
        public Camera rightEye;

        private void Start()
        {
            rightEye = GameObject.FindGameObjectWithTag("Aryzon").transform.Find("Right").GetComponent<Camera>();
            buttonCanvas.worldCamera = rightEye;
        }

        /// <summary>
        /// The Unity Update method.
        /// </summary>
        public void Update()
        {
            //if (Image == null || Image.TrackingState != TrackingState.Tracking)
            //{
            //    buttonCanvas.SetActive(false);
            //    return;
            //}
            //buttonCanvas.SetActive(true);
        }

        public void CreateCenterpoint()
        {
            GameObject.FindGameObjectWithTag("Aryzon").GetComponent<AryzonTracking>().StartAryzonMode();
            Instantiate(centerObjectPrefab, thisAnchor.transform);
        }

    }
}
