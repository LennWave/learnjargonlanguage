﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject Bolletje;

    void Start()
    {
        Debug.Log("Load object " + PlayerPrefs.GetString("SelectedGameData", "Vliegtuig"));
        var res = Resources.Load<JargonContainer>( PlayerPrefs.GetString("SelectedGameData", "Vliegtuig"));
        Debug.Log(res);

        GameObject obj = PhotonNetwork.Instantiate( res.Model.name, Vector3.zero,Quaternion.identity,0);  // GameObject.Instantiate(res.Model, this.transform);

        if(obj == null)
        {
            Debug.LogError("Er ging iets mis");
        }

    }

}
