﻿using System.Collections.Generic;
using UnityEngine;

public class JargonContainer : ScriptableObject
{
    public GameObject Model;

    public string ModelName;

    [SerializeField]
    public List<JargonMapping> Mapping = new List<JargonMapping>();

}
