﻿using System;

[Serializable]
public class JargonData
{
    public JargonData(string _commonID)
    {
        CommonID = _commonID;
    }

    public JargonData(JargonData clone)
    {
        CommonID = clone.CommonID;
        Name = clone.Name;
        Description = clone.Description;
    }

    public string CommonID;

    //public string UniqueID;

    public string Name;

    public string Description;


}
