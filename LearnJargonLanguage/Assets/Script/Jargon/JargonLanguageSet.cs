﻿using System.Collections.Generic;
using UnityEngine;

public class JargonLanguageSet : ScriptableObject
{
    private static JargonLanguageSet _defaultLanguageSet;
    public static JargonLanguageSet DefaultLanguageSet
    {
        get
        {
            //nicley hardcoded. :D
            if (_defaultLanguageSet == null)
                _defaultLanguageSet = Resources.Load("Assets/PhotonResources/LanguageDatabase/Resources/English.asset") as JargonLanguageSet;
            return _defaultLanguageSet;
        }
    }

    //public 
    public string LanguageId;

    public string LanguageName;

    [SerializeField]
    public List<JargonData> Terms = new List<JargonData>();

}
