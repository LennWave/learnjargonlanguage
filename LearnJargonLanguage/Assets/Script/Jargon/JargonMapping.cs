﻿using System;
using UnityEngine;

[Serializable]
public class JargonMapping
{

    public JargonMapping()
    {
        UniqueId = Guid.NewGuid().ToString();
    }

    public string CommonID;

    public string UniqueId;

    public float PosX;
    public float PosY;
    public float PosZ;

    public Vector3 Location
    {
        get
        {
            return new Vector3(PosX, PosY, PosZ);
        }
        set
        {
            PosX = value.x;
            PosY = value.y;
            PosZ = value.z;
        }
    }

}
