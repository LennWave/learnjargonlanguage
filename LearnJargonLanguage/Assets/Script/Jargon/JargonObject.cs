﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class JargonObject : MonoBehaviour
{

    public JargonContainer Data;
    private List<GameObject> MappingDots;

    [ExecuteInEditMode]
    public void Initialize(JargonContainer _data)
    {
        Data = _data;
        if (Data == null)
        {
            Debug.LogError("Data was null");
            return;
        }

        for (int i = 0; i < Data.Mapping.Count; i++)
        {
            var mapping = Data.Mapping[i];
            if (mapping == null)
            {
                Debug.LogError("Mapping is null");
                continue;
            }

            var term = JargonLanguageSet.DefaultLanguageSet.Terms.Where(x => x.CommonID == mapping.CommonID).FirstOrDefault();
            if (term == null)
            {
                Debug.LogError("no term found for id " + mapping.CommonID);
                continue;
            }

            GameObject map = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            map.transform.SetParent(this.transform);

            map.name = term.Name + mapping.UniqueId;
            map.transform.localPosition = mapping.Location;
        }

    }


}
