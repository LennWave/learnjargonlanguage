﻿using BreinWave.Game;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectController : MonoBehaviour
{
    public int currentContainer = 0;

    public float speed = 20;

    public GameObject currentContainerObject;
    public GameObject nextContainerObject;
    public GameObject previousContainerObject;

    public List<JargonContainer> Containers;
    public List<GameObject> Previews = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        UIFollow follower = this.GetComponent<UIFollow>();
        // set this UI to follow the player.
        follower.Target = GameManager.Instance.CurrentDevice.GetVirtualDevice().transform;

#if UNITY_EDITOR

#endif
        Resources.LoadAll("");
        var LoadResources = Resources.FindObjectsOfTypeAll(typeof(JargonContainer)) as JargonContainer[];

        Containers = LoadResources.ToList();
        Debug.Log("Assets loaded : " + Containers.Count);
        currentContainer = 0;

        for (int i = 0; i < Containers.Count; i++)
        {
            JargonContainer data = Containers[i];
            GameObject obj = Instantiate(data.Model, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            obj.name = data.ModelName;
            obj.transform.localPosition = new Vector3(0, 0, 0);
            obj.transform.localScale = Vector3.one;
            Previews.Add(obj);
        }

        SetContainerVisuals();
    }

    void Update()
    {
        // Spin Y axis at the same speed.
        //currentContainerObject.transform.Rotate(currentContainerObject.transform.right, speed * Time.deltaTime); //.Rotate(0, speed * Time.deltaTime, 0, Space.World);
        currentContainerObject.transform.Rotate(0, speed * Time.deltaTime, 0, Space.Self);
        previousContainerObject.transform.Rotate(0, speed * Time.deltaTime, 0, Space.Self);
        nextContainerObject.transform.Rotate(0, speed * Time.deltaTime, 0, Space.Self);
    }

    public int GetCurrentOffset(int offset)
    {
        if (currentContainer + offset >= Containers.Count)
        {
            return 0;
        }
        else if (currentContainer + offset < 0)
        {
            return Containers.Count - 1;
        }

        return currentContainer + offset;
    }

    public void OnSelectContainer()
    {
        Debug.Log("You have selected " + Containers[currentContainer].ModelName);

        PlayerPrefs.SetString("SelectedGameData", Containers[currentContainer].name);

        SceneManager.LoadScene("GameScene");
    }

    public void OnSelectContainerMultiplayer()
    {

        PlayerPrefs.SetString("SelectedGameData", Containers[currentContainer].name);

        SceneManager.LoadScene("Lobby_Scene");
        
    }

    public void NextContainer()
    {
        currentContainer = GetCurrentOffset(+1);
        SetContainerVisuals();
    }

    public void PreviousContainer()
    {
        currentContainer = GetCurrentOffset(-1);
        SetContainerVisuals();
    }

    public void SetContainerVisuals()
    {
        for (int i = 0; i < Containers.Count; i++)
        {
            GameObject obj = Previews[i];
            obj.SetActive(true);

            if (i == currentContainer)
            {
                obj.transform.SetParent(currentContainerObject.transform);
            }
            else if (i == GetCurrentOffset(-1))
            {
                obj.transform.SetParent(previousContainerObject.transform);
            }
            else if (i == GetCurrentOffset(1))
            {
                obj.transform.SetParent(nextContainerObject.transform);
            }
            else
            {
                obj.SetActive(false);
            }

            obj.transform.localPosition = new Vector3(0, 0, 0);
            obj.transform.localScale = Vector3.one;
        }
    }


}
