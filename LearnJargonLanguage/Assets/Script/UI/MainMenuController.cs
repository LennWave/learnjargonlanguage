﻿using BreinWave.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    public void Start()
    {
        UIFollow follower = this.GetComponent<UIFollow>();
        // set this UI to follow the player.
        follower.Target = GameManager.Instance.CurrentDevice.GetVirtualDevice().transform;
    }

    public void OnLevelSelectButton()
    {
        SceneManager.LoadScene("LevelSelect");
    }
}
