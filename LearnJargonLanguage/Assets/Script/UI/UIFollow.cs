﻿using UnityEngine;

public class UIFollow : MonoBehaviour
{

    public bool DoFollow = true;

    public Transform Target;

    //public Vector3 Offset = new Vector3(0,0,4);
    public Vector2 RotationalOffset;

    private Vector3 deltaPos;

    public float smoothing = 4f;

    public void Update()
    {
        if (!DoFollow)
            return;

        if (Target == null)
            return;


        //move position
        this.transform.position = Vector3.Lerp(deltaPos, Target.transform.position, Time.deltaTime * smoothing);// + Offset;


        Quaternion diff =  Quaternion.Euler(new Vector3(Target.transform.eulerAngles.x - transform.eulerAngles.x, Target.transform.eulerAngles.y - transform.eulerAngles.y, 0));
        if (Mathf.Abs(diff.y) > RotationalOffset.y || Mathf.Abs(diff.x) > RotationalOffset.x)
        {
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(Target.transform.eulerAngles.x, Target.transform.eulerAngles.y,0), Time.deltaTime * smoothing);
        }

        deltaPos = this.transform.position;
    }
}
